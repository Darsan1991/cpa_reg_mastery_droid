package fiverr.com.cparegmastery.others;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;

import fiverr.com.cparegmastery.R;
import fiverr.com.cparegmastery.activity.FreeVersionActivity;

/**
 * Created by Darsan on 5/2/2017.
 */

public class PremiumPopUp implements View.OnClickListener{

    private Context context;
    private Dialog dialog;

    public PremiumPopUp(Context context) {
        this.context = context;
        AlertDialog.Builder mBuilder = new AlertDialog.Builder(context);
        LayoutInflater inflater = LayoutInflater.from(context);
        View mLayout = inflater.inflate(R.layout.premium_pop_up,null);
        mLayout.findViewById(R.id.may_be_later_btn).setOnClickListener(this);
        mLayout.findViewById(R.id.unlock_premium_btn).setOnClickListener(this);
        mBuilder.setView(mLayout);
        dialog = mBuilder.create();

    }

    public void show()
    {
        dialog.show();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.may_be_later_btn:
                dialog.dismiss();
                break;

            case R.id.unlock_premium_btn:
                Intent intent = new Intent(context, FreeVersionActivity.class);
                context.startActivity(intent);
                dialog.dismiss();
                break;
        }
    }
}
