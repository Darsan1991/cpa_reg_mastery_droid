package fiverr.com.cparegmastery.extensions;

import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.view.ViewGroup;

import java.util.HashMap;

/**
 * Created by Darsan on 4/28/2017.
 */

public abstract class FragmentStatePagerAdapterWithFragment<T> extends FragmentStatePagerAdapter {
    private HashMap<Integer,T> regirestedFragmentHashMap = new HashMap<>();

    public FragmentStatePagerAdapterWithFragment(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        T t = (T)super.instantiateItem(container, position);
        regirestedFragmentHashMap.put(position,t);
        return t;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        super.destroyItem(container, position, object);
        regirestedFragmentHashMap.remove(position);
    }

    public T getRegistredFragment(int position)
    {
        if(regirestedFragmentHashMap.containsKey(position))
            return regirestedFragmentHashMap.get(position);

        return null;
    }
}
