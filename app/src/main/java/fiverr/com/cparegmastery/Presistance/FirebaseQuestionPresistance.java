package fiverr.com.cparegmastery.Presistance;

import android.support.annotation.NonNull;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

import fiverr.com.cparegmastery.QuestionProgressDto;
import fiverr.com.cparegmastery.interfaces.OnTaskCompleteListner;
import fiverr.com.cparegmastery.interfaces.QuestionProgressPresistance;

/**
 * Created by Darsan on 5/12/2017.
 */

public class FirebaseQuestionPresistance implements QuestionProgressPresistance {

    private DatabaseReference databaseReference;
    public final static String idPrefix = "question_";
    private static List<OnSyncListner> onSyncListnerList = new ArrayList<>();

    public FirebaseQuestionPresistance()
    {
       databaseReference = FirebaseDatabase.getInstance()
                .getReference("users")
               .child(FirebaseAuth.getInstance().getCurrentUser().getUid())
               .child("QuestionProgresses");
    }

    @Override
    public void loadAllQuestions(final OnLoadAllQuestionsListner listner) {
        databaseReference.keepSynced(true);
        databaseReference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                List<QuestionProgressDto> questionProgressDtoList = new ArrayList<QuestionProgressDto>();
                if(dataSnapshot.getChildrenCount() > 0)
                {
                    Iterable<DataSnapshot> childrens = dataSnapshot.getChildren();
                    for (DataSnapshot children : childrens) {
                        Object idObj = children.child("id").getValue();
                        Object progressObj = children.child("progressType").getValue();
                        int id =Integer.parseInt(idObj.toString());
                        int progress =Integer.parseInt(progressObj.toString());
                        boolean isBookMarked =(boolean)children.child("isBookMarked").getValue();
                        boolean isCorrect =(boolean)children.child("isCorrect").getValue();
                        questionProgressDtoList.add(new QuestionProgressDto(id,isBookMarked,progress,isCorrect));
                    }

                }
                if(listner!=null)
                    listner.onLoaded(questionProgressDtoList);
                databaseReference.keepSynced(false);
                syncCall();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    @Override
    public void updateQuestionProgress(int id, final QuestionProgressDto progress, final OnUpdateListner listner) {
        databaseReference.child(idPrefix+id).setValue(progress).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if(listner!=null)
                    listner.onUpdateFinished(progress);

                if(task.isSuccessful())
                {
                    syncCall();
                }
            }
        });
    }

    @Override
    public void loadQuestionProgress(int id, final OnLoadQuestionLisner listner) {
        databaseReference.child(idPrefix+id).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if(dataSnapshot!=null && listner!=null)
                    listner.onLoaded(dataSnapshot.getValue(QuestionProgressDto.class));
                    syncCall();

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    @Override
    public void reset(final OnTaskCompleteListner listner) {
        databaseReference.removeValue().addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if(listner!=null)
                    listner.onComplete(task.isSuccessful()? OnTaskCompleteListner.Result.SUCCESS: OnTaskCompleteListner.Result.FAILED);

                if(task.isSuccessful())
                {
                    syncCall();
                }
            }
        });
    }

    @Override
    public void addOnStatusChangedListner(OnSyncListner onSyncListner) {
        onSyncListnerList.add(onSyncListner);
    }

    @Override
    public void removeOnStatusChangedListner(OnSyncListner onSyncListner) {
        onSyncListnerList.remove(onSyncListner);
    }

    private void syncCall()
    {
        for (int i = 0; i < onSyncListnerList.size(); i++) {
            OnSyncListner onSyncListner = onSyncListnerList.get(i);
            if(onSyncListner == null) {
                onSyncListnerList.remove(i);
                i--;
                continue;
            }
            onSyncListner.onSync();
        }
    }
}
