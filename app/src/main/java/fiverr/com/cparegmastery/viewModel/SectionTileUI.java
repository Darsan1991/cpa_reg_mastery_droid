package fiverr.com.cparegmastery.viewModel;

import java.util.List;

import fiverr.com.cparegmastery.model.QuestionProgress;
import fiverr.com.cparegmastery.model.Section;

/**
 * Created by Darsan on 5/8/2017.
 */

public class SectionTileUI {
    public int sectionId;
    public Section section;
    public String title;
    public List<QuestionProgress> questionProgresses;

    public SectionTileUI(Section section, List<QuestionProgress> questionProgresses) {
        this.section = section;
        this.questionProgresses = questionProgresses;
        title = section.getTitle();
        sectionId = section.getId();
    }
}
