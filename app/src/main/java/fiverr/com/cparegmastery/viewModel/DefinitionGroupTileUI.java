package fiverr.com.cparegmastery.viewModel;

import java.util.ArrayList;
import java.util.List;

import fiverr.com.cparegmastery.model.Definition;

/**
 * Created by Darsan on 5/2/2017.
 */

public class DefinitionGroupTileUI {

    private String title;
    private List<Definition> definitions = new ArrayList<>();



    public DefinitionGroupTileUI(String title, List<Definition> definitions) {
        this.title = title;
        this.definitions = definitions;

    }

    public List<Definition> getDefinitions() {
        return definitions;
    }

    public String getTitle() {
        return title;
    }

}
