package fiverr.com.cparegmastery.viewModel;

/**
 * Created by Darsan on 4/11/2017.
 */

public class HomeTileUI {

    private String title;
    private int iconResourceID;
    private boolean isLocked;
    private Type type;
    private Object object;

    public HomeTileUI(String title, int iconResourceID, boolean isLocked, Type type) {
        this.title = title;
        this.iconResourceID = iconResourceID;
        this.isLocked = isLocked;
        this.type = type;
    }

    public HomeTileUI(String title, int iconResourceID, boolean isLocked, Type type, Object extraObject) {
        this.title = title;
        this.iconResourceID = iconResourceID;
        this.isLocked = isLocked;
        this.type = type;
        this.object = extraObject;
    }

    public Object getExtraObject() {
        return object;
    }

    public String getTitle() {
        return title;
    }

    public int getIconResourceID() {
        return iconResourceID;
    }


    public boolean isLocked() {

        return isLocked;
    }

    public Type getType() {
        return type;
    }

    public enum Type{
        SUBJECT,DEFINITION,PROGRESS
    }

}
