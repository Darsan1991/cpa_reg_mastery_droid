package fiverr.com.cparegmastery.viewModel;

import fiverr.com.cparegmastery.Adapter.QuestionsProgressAdapter;

/**
 * Created by Darsan on 4/30/2017.
 */

public class ProgressTileUI {
    private String title;
    private int percentage;
    private QuestionsProgressAdapter progressAdapter;
    private boolean isAnyQuestionAnswred;


    public ProgressTileUI(String title, int percentage, QuestionsProgressAdapter progressAdapter, boolean isAnyQuestionAnswred) {
        this.title = title;
        this.percentage = percentage;
        this.progressAdapter = progressAdapter;
        this.isAnyQuestionAnswred = isAnyQuestionAnswred;
    }

    public QuestionsProgressAdapter getProgressAdapter() {
        return progressAdapter;
    }

    public String getTitle() {
        return title;
    }

    public int getPercentage() {
        return percentage;
    }

    public boolean isAnyQuestionAnswred() {
        return isAnyQuestionAnswred;
    }
}
