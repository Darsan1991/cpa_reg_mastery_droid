package fiverr.com.cparegmastery.activity;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.facebook.CallbackManager;
import com.google.firebase.auth.FirebaseAuth;

import fiverr.com.cparegmastery.R;
import fiverr.com.cparegmastery.fragment.account.SignupFragment;
import fiverr.com.cparegmastery.interfaces.FragmentContainer;

public class LoginActivity extends AppCompatActivity implements FragmentContainer{

    private static String TAG= LoginActivity.class.getSimpleName();
    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthListener;
    private CallbackManager mCallbackManager;
    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        SignupFragment signupFragment = new SignupFragment();
        getSupportFragmentManager()
                .beginTransaction()
                .replace(getContainerId(),signupFragment)
                .commit();
        getSupportFragmentManager().executePendingTransactions();
    }

    @Override
    public int getContainerId() {
        return R.id.contentPanel;
    }

}
