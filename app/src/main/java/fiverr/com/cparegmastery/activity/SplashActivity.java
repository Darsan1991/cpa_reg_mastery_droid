package fiverr.com.cparegmastery.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.facebook.CallbackManager;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import fiverr.com.cparegmastery.R;
import fiverr.com.cparegmastery.interfaces.TaskStatable;
import fiverr.com.cparegmastery.services.GlobalService;
import fiverr.com.cparegmastery.services.InformationService;
import fiverr.com.cparegmastery.services.QuestionService;

public class SplashActivity extends AppCompatActivity {

    private static String TAG= SplashActivity.class.getSimpleName();
    private FirebaseAuth mAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        mAuth = FirebaseAuth.getInstance();
    }




    @Override
    protected void onResume() {
        super.onResume();
        if(mAuth.getCurrentUser()==null)
        {
            FirebaseUser currentUser = mAuth.getCurrentUser();
            if(currentUser==null)
            {
                Intent intent = new Intent(this,LoginActivity.class);
                startActivity(intent);
                return;
            }
            return;
        }

        //start the service

        QuestionService.startService(this);
        GlobalService.startService(this);
        if(QuestionService.getService()!=null && QuestionService.getService().getState() == TaskStatable.State.FINISHED)
            QuestionService.getService().referesh(null);
        if(GlobalService.getService()!=null && GlobalService.getService().getState()== TaskStatable.State.FINISHED)
            GlobalService.getService().referesh(null);

        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(1000);

                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                while(InformationService.getService().getState()!= TaskStatable.State.FINISHED
                        || QuestionService.getService().getState()!= TaskStatable.State.FINISHED
                        || GlobalService.getService().getState()!= TaskStatable.State.FINISHED)
                {
                    try {
                        Thread.sleep(100);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Intent intent = new Intent(getApplicationContext(),MainActivity.class);
                        startActivity(intent);
                        finish();
                    }
                });
            }
        };

        new Thread(runnable).start();
    }
}
