package fiverr.com.cparegmastery.activity;

import android.animation.Animator;
import android.animation.AnimatorInflater;
import android.animation.AnimatorSet;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import fiverr.com.cparegmastery.Adapter.QuestionsProgressAdapter;
import fiverr.com.cparegmastery.R;
import fiverr.com.cparegmastery.fragment.quiz.QuizFragment;
import fiverr.com.cparegmastery.fragment.quiz.ResultFragment;
import fiverr.com.cparegmastery.interfaces.QuestionProvider;
import fiverr.com.cparegmastery.model.Question;
import fiverr.com.cparegmastery.model.QuestionProgress;
import fiverr.com.cparegmastery.services.GlobalService;
import fiverr.com.cparegmastery.services.QuestionService;
import fiverr.com.cparegmastery.utils.Utils;
import fiverr.com.cparegmastery.view.MultiProgressBar;

public class QuizActivity extends AppCompatActivity implements QuizFragment.QuizFragmentListner,ResultFragment.OnFragmentInteractionListener {
    public final static String QUESTIONS_KEY="Questions";
    private static final String TAG = QuizActivity.class.getSimpleName();
    private ArrayList<Integer> questionProgressArrayList = new ArrayList<>(Arrays.asList(1,2,3,4,5));
    private MultiProgressBar multiProgressBar;
    private TextView noIndicatorTV;

    private QuestionsProgressAdapter progressAdapter;
    private QuizFragment quizFragment;
    QuestionProvider questionService;
    private GlobalService globalService;

    private Question currentQuestion;
    private Menu menu;
    private int lastSelectedAnswerIndex;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quiz);

        globalService = GlobalService.getService();
        questionService = QuestionService.getService();
        multiProgressBar = (MultiProgressBar)findViewById(R.id.multiprogress_bar);
        noIndicatorTV = (TextView)findViewById(R.id.no_indicator_tv);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        questionProgressArrayList = getIntent().getIntegerArrayListExtra(QUESTIONS_KEY);
        quizFragment = QuizFragment.getIntance(questionProgressArrayList);

        progressAdapter = new QuestionsProgressAdapter(questionService.getQuestionProgressForIDs(questionProgressArrayList));
        multiProgressBar.setAdapter(progressAdapter, MultiProgressBar.AnimateType.BEGIN);

        getSupportFragmentManager().beginTransaction().add(R.id.content_quiz_panel,quizFragment).commit();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        this.menu = menu;
        getMenuInflater().inflate(R.menu.quiz_menu,menu);

        //remove if premium version
        if(globalService.isPremiumVersion())
            menu.removeItem(R.id.what_is_upgrade_btn);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId())
        {
            case android.R.id.home:
                onBackPressed();
                return true;

            case R.id.bookmark_btn:
                toggleBookMark();
                break;
            case R.id.profile_btn:
                ProfileActivity.openActivity(this);
                break;
            case R.id.menu_item_share:
                share();
                break;
            case R.id.talk_to_expert:
                Utils.General.talkToExpert(this);
                break;

        }

        return super.onOptionsItemSelected(item);
    }

    private void share()
    {
        QuestionProgress question = questionService.getQuestionForID(quizFragment.getCurrentQuestionId());
        boolean hasPermissionToWrite = Utils.General.hasPermissionToWrite(this);
        Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);

        Log.i(TAG,"has Permission to write :"+hasPermissionToWrite);
        if(hasPermissionToWrite) {
            ViewGroup rootView = (ViewGroup) ((ViewGroup) this
                    .findViewById(android.R.id.content)).getChildAt(0);
            Uri uri = Uri.fromFile(Utils.General.saveBitmap(Utils.General.getScreenShot(rootView),"screenshot.png"));
            sharingIntent.setType("image/*");
            sharingIntent.putExtra(Intent.EXTRA_STREAM, uri);
        }
        else
        {
            sharingIntent.setType("text/*");
        }

        String shareBody = question.getQuestion() + "\n\n"
                +question.getOptions()[0]+"\n\n"
                +question.getOptions()[1]+"\n\n"
                +question.getOptions()[2]+"\n\n"
                +question.getOptions()[3]+"\n"
                ;
        sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, question.getSection().getTitle());
        sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareBody);


        startActivity(Intent.createChooser(sharingIntent, "Share via"));
    }

    private void toggleBookMark()
    {
        int questionId = quizFragment.getCurrentQuestionId();
        QuestionProgress questionProgress = questionService.getQuestionForID(questionId);
        questionService.setAsBookMark(questionProgress.getId(),!questionProgress.isBookMarked());
        refereshBookMarkMenu();
    }

    private void refereshBookMarkMenu()
    {
        if(menu==null) {
            Runnable runnable = new Runnable() {
                @Override
                public void run() {
                    while(menu==null)
                    {

                    }
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            refereshBookMarkMenu();
                        }
                    });
                }
            };

            new Thread(runnable).start();

            return;
        }


        int questionId = quizFragment.getCurrentQuestionId();
        QuestionProgress questionProgress = questionService.getQuestionForID(questionId);
        menu.findItem(R.id.bookmark_btn).setIcon(questionProgress.isBookMarked()?R.drawable.ic_star:R.drawable.ic_star_border);
    }

    public List<Integer> getQuestionList() {
        return questionProgressArrayList;
    }

    @Override
    public void onSubmitAnswer(Question question, int selectedIndex) {
        setUpAndShowResultFragment(question,selectedIndex);
        currentQuestion = question;
        this.lastSelectedAnswerIndex = selectedIndex;

    }

    @Override
    public void onButtonClicked(ResultFragment.ButtonType buttonType) {
        questionService.setAsCorrect(currentQuestion.getId(),lastSelectedAnswerIndex == currentQuestion.getCorrectAnswerIndex());
        questionService.setProgressType(currentQuestion.getId(),ResultFragment.buttonTypeToProgressType(buttonType));

        if(quizFragment.hasNextQuiz()) {

            hideTheResultAndShowNextQuiz();

            progressAdapter.referesh();
        }
        else
        {
            finish();
        }
    }

    @Override
    public void onChangedQuestion(int index) {
        noIndicatorTV.setText((index+1) + " of "+questionProgressArrayList.size());
        setTitleByQuestion(index);
        refereshBookMarkMenu();
    }

    private void setTitleByQuestion(int index)
    {
        QuestionProgress questionProgress = questionService.getQuestionForID(questionProgressArrayList.get(index));
        getSupportActionBar().setTitle(questionProgress.getSection().getTitle());
    }

    private void setUpAndShowResultFragment(Question question, int selectedIndex)
    {
        final View quizPannel = findViewById(R.id.content_quiz_panel);
        View resultPannel = findViewById(R.id.content_result_panel);

        quizPannel.setEnabled(false);
        resultPannel.setEnabled(false);

        ResultFragment resultFragment = ResultFragment.getInstance(question.getId(),selectedIndex);
        getSupportFragmentManager().beginTransaction().add(R.id.content_result_panel,resultFragment).commit();
        AnimatorSet animatorSet = (AnimatorSet) AnimatorInflater.loadAnimator(getApplicationContext(),R.animator.card_flip_right_out);
        animatorSet.setTarget(quizPannel);
        animatorSet.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {

            }

            @Override
            public void onAnimationEnd(Animator animator) {
                quizPannel.setVisibility(View.GONE);
            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        });
        animatorSet.start();

        animatorSet = (AnimatorSet) AnimatorInflater.loadAnimator(getApplicationContext(),R.animator.card_flip_right_in);
        animatorSet.setTarget(resultPannel);
        animatorSet.start();
    }



    private void hideTheResultAndShowNextQuiz()
    {
        getSupportFragmentManager().beginTransaction().remove(getSupportFragmentManager().findFragmentById(R.id.content_result_panel)).commit();
        View quizPannel = findViewById(R.id.content_quiz_panel);
        quizPannel.setRotationY(0);
        quizPannel.setAlpha(1);
        quizPannel.setVisibility(View.VISIBLE);
        quizFragment.clearTheCurrentAndGoToNextIndex();
    }
}
