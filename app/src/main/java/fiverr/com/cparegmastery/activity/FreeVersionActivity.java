package fiverr.com.cparegmastery.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;

import fiverr.com.cparegmastery.R;
import fiverr.com.cparegmastery.fragment.freeversion.FreeFragment;
import fiverr.com.cparegmastery.fragment.freeversion.PremiumFragment;

public class FreeVersionActivity extends AppCompatActivity implements View.OnClickListener,ViewPager.OnPageChangeListener {

    private View freeNormalGroup,freeSelectedGroup,premiumNormalGroup,premiumSelectedGroup;
    private ViewPager viewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_free_version);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        freeNormalGroup = findViewById(R.id.free_group_normal);
        freeSelectedGroup = findViewById(R.id.free_group_selected);
        premiumNormalGroup = findViewById(R.id.premium_group_normal);
        premiumSelectedGroup = findViewById(R.id.premium_group_selected);

        findViewById(R.id.free_group).setOnClickListener(this);
        findViewById(R.id.premium_group).setOnClickListener(this);

        viewPager = (ViewPager) findViewById(R.id.viewPager);
        viewPager.setAdapter(new VersionPageAdapter(getSupportFragmentManager()));
        viewPager.addOnPageChangeListener(this);
        onPageSelected(0);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.free_group:
                    viewPager.setCurrentItem(0,true);
                break;

            case R.id.premium_group:
                viewPager.setCurrentItem(1,true);
                break;
        }
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        freeNormalGroup.setVisibility(position==0?View.GONE:View.VISIBLE);
        freeSelectedGroup.setVisibility(position==0?View.VISIBLE:View.GONE);
        premiumSelectedGroup.setVisibility(position==0?View.GONE:View.VISIBLE);
        premiumNormalGroup.setVisibility(position==0?View.VISIBLE:View.GONE);
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId())
        {
            case android.R.id.home:
                onBackPressed();

                return true;
        }
        return super.onOptionsItemSelected(item);

    }

    public class VersionPageAdapter extends FragmentPagerAdapter
    {

        public VersionPageAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            return  position==0?new FreeFragment():new PremiumFragment();
        }

        @Override
        public int getCount() {
            return 2;
        }
    }


    public static void openActivity(Context context)
    {
        Intent intent = new Intent(context,FreeVersionActivity.class);
        context.startActivity(intent);
    }


}
