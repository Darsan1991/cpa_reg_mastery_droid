package fiverr.com.cparegmastery.activity;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import fiverr.com.cparegmastery.AccountHandler;
import fiverr.com.cparegmastery.FirebaseAccountHandler;
import fiverr.com.cparegmastery.R;
import fiverr.com.cparegmastery.interfaces.OnTaskCompleteListner;
import fiverr.com.cparegmastery.services.GlobalService;
import fiverr.com.cparegmastery.utils.FirebaseUtils;
import fiverr.com.cparegmastery.utils.Utils;

public class ProfileActivity extends AppCompatActivity implements View.OnClickListener {

    TextView emailTV,syncTV;
    AccountHandler accountHandler;
    private ProgressDialog progressDialog;
    GlobalService globalService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setContentView(R.layout.activity_profile);
        globalService = GlobalService.getService();

        accountHandler = new FirebaseAccountHandler();
        emailTV = (TextView) findViewById(R.id.email_address);
        syncTV = (TextView) findViewById(R.id.sync_tv);
        showSyncTime(globalService.lastSyncedTime());

        emailTV.setText(accountHandler.getEmailAddress());

        findViewById(R.id.reset_password_btn).setOnClickListener(this);
        findViewById(R.id.sync_progress_btn).setOnClickListener(this);
        findViewById(R.id.reset_process_btn).setOnClickListener(this);
        findViewById(R.id.sign_out_btn).setOnClickListener(this);

    }


    private void showSyncTime(long lastSyncTime)
    {
        long seconds = (System.currentTimeMillis()-globalService.lastSyncedTime())/1000;
        String text = "";
        if(seconds/(3600*24)>0)
        {
            text = (seconds/(3600*24)) + " days ago";
        }
        else if(seconds/3600 > 0)
        {
            text = (seconds/3600) + " hours ago";
        }
        else if(seconds / 60 >0)
        {
            text = (seconds/60) + " minutes ago";
        }
        else
        {
            text = seconds + " seconds ago";
        }
        syncTV.setText(text);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId())
        {
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    public static void openActivity(Context context)
    {
        Intent intent = new Intent(context,ProfileActivity.class);
        context.startActivity(intent);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.sync_progress_btn:
                syncProcess();
                break;
            case R.id.reset_password_btn:
                resetPassword();
                break;
            case R.id.reset_process_btn:
                    resetProcess();
                break;
            case R.id.sign_out_btn:
                signOut();
                break;
        }

    }


    private void syncProcess()
    {


        if(!Utils.NetworkUtils.hasInternetConnection(this))
        {
            AlertDialog.Builder mBuilder = new AlertDialog.Builder(this);
            mBuilder.setTitle("No Internet Connection!").setMessage("Please turn on internet connection and try again").setPositiveButton("OK",null).show();

        }
        else {
            showProgressDialog("Syncing","waiting for sync");
            FirebaseUtils.syncDatabase(new OnTaskCompleteListner() {
                @Override
                public void onComplete(Result result) {

                    Intent intent = new Intent(getApplicationContext(), SplashActivity.class);
                    startActivity(intent);
                    hideProgressDialog();
                }
            });
        }
    }

    private void resetProcess()
    {
        showProgressDialog("Resetting","waiting for reset progress");
        accountHandler.resetAccount(new OnTaskCompleteListner() {
            @Override
            public void onComplete(Result result) {
                hideProgressDialog();

                Intent intent = new Intent(getApplicationContext(),SplashActivity.class);
                startActivity(intent);
            }
        });
    }


    private void signOut()
    {
        showProgressDialog("Sign Out","waiting for sign out");
        accountHandler.signOut();
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Intent intent = new Intent(getApplicationContext(),SplashActivity.class);
                        startActivity(intent);
                        finish();
                        hideProgressDialog();
                    }
                });
            }
        };

        new Thread(runnable).start();


    }

    private void showProgressDialog(String title,String content)
    {
        progressDialog = ProgressDialog.show(this,title,content);
    }

    private void hideProgressDialog()
    {
        progressDialog.dismiss();
    }

    private void resetPassword()
    {
        AlertDialog.Builder mBuilder = new AlertDialog.Builder(this);
        LayoutInflater inflater = LayoutInflater.from(this);
        final View mLayout = inflater.inflate(R.layout.reset_password_popup,null);
        mBuilder.setView(mLayout)
                .setPositiveButton("Change", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                showProgressDialog("Resetting Password","Waiting for Reset password");
               EditText editText = (EditText) mLayout.findViewById(R.id.password);
                accountHandler.updatePassword(editText.getText().toString(), new OnTaskCompleteListner() {
                    @Override
                    public void onComplete(Result result) {
                        hideProgressDialog();
                        Toast.makeText(getApplicationContext(),result==Result.SUCCESS?"Succefully Password changed!":"Something went wrong",Toast.LENGTH_LONG).show();
                    }
                });
            }
        }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        }).show();

    }
}
