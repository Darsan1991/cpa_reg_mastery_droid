package fiverr.com.cparegmastery.activity;

import android.Manifest;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import fiverr.com.cparegmastery.R;
import fiverr.com.cparegmastery.fragment.HomeFragment;
import fiverr.com.cparegmastery.fragment.HomeTabbarFragment;
import fiverr.com.cparegmastery.fragment.NameCompactFragment;
import fiverr.com.cparegmastery.fragment.ProgressFragment;
import fiverr.com.cparegmastery.fragment.definition.DefinitionsFragment;
import fiverr.com.cparegmastery.interfaces.InformationProvider;
import fiverr.com.cparegmastery.model.Definition;
import fiverr.com.cparegmastery.services.GlobalService;
import fiverr.com.cparegmastery.services.InformationService;
import fiverr.com.cparegmastery.utils.Utils;

public class MainActivity extends AppCompatActivity implements ViewPager.OnPageChangeListener,FragmentManager.OnBackStackChangedListener,HomeTabbarFragment.HomeTabbarFragmentListner {

    private static final int MULTIPLE_PERMISSIONS = 100;
    private String[] permissions= new String[]{
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.READ_EXTERNAL_STORAGE
           };

    private ViewPager pager;
    private MainFragmentPageAdapter mainFragmentPageAdapter;
    private InformationProvider informationProvider;
    private GlobalService globalService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        globalService = GlobalService.getService();

        pager = (ViewPager) findViewById(R.id.viewPager);
        pager.setOffscreenPageLimit(2);
        HomeTabbarFragment fragment = (HomeTabbarFragment) getSupportFragmentManager().findFragmentById(R.id.tab);
        fragment.setViewPager(pager);

        fragment.showUnLockButton(!globalService.isPremiumVersion());
        pager.addOnPageChangeListener(this);
        informationProvider = InformationService.getService();
        mainFragmentPageAdapter = new MainFragmentPageAdapter(getSupportFragmentManager());
        pager.setAdapter(mainFragmentPageAdapter);


        if (checkPermissions()) {
            
        }
        //  permissions  granted.


    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        return true;
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
       refreshMenuAndTitle();

    }


    private void refreshMenuAndTitle()
    {
        NameCompactFragment fragment = mainFragmentPageAdapter.getFragmentAt(pager.getCurrentItem());
        String title = fragment.getTitle();
        getSupportActionBar().setDisplayHomeAsUpEnabled(fragment.getChildFragmentManager().getBackStackEntryCount()>0);

        List<Fragment> fragmentList = fragment.getChildFragmentManager().getFragments();
        if(fragmentList != null && fragmentList.size()>0) {
            Fragment selectedFragment = null;
            for (int i=fragmentList.size()-1;i>=0;i--)
            {
                if(fragmentList.get(i)!=null && (fragmentList.get(i) instanceof NameCompactFragment))
                {
                    selectedFragment = fragmentList.get(i);
                    break;
                }
            }
            if(selectedFragment!=null )
                title = ((NameCompactFragment)selectedFragment).getTitle();

        }
        getSupportActionBar().setTitle(title);
        invalidateOptionsMenu();
    }


    private  boolean checkPermissions() {
        int result;
        List<String> listPermissionsNeeded = new ArrayList<>();
        for (String p:permissions) {
            result = ContextCompat.checkSelfPermission(this,p);
            if (result != PackageManager.PERMISSION_GRANTED) {
                listPermissionsNeeded.add(p);
            }
        }
        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(this, listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]),MULTIPLE_PERMISSIONS );
            return false;
        }
        return true;
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MULTIPLE_PERMISSIONS:{
                if(grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED){
                    // permissions granted.
                } else {
                  checkPermissions();
                }
                return;
            }
        }
    }


    @Override
    public void onPageScrollStateChanged(int state) {

    }

    @Override
    public void onBackPressed() {

        Fragment fragment = mainFragmentPageAdapter.getFragmentAt(pager.getCurrentItem());
        if(fragment==null || fragment.getChildFragmentManager().getBackStackEntryCount()==0) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle("Are you sure want to exit?").setMessage("Press ok to exit the app").setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    finish();
                }
            }).setNegativeButton("Cancel",null).show();

            return;
        }
        fragment.getChildFragmentManager().popBackStack();
    }

    @Override
    public void onBackStackChanged() {
        refreshMenuAndTitle();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId())
        {
            case android.R.id.home:
                onBackPressed();
                break;
            case R.id.profile_btn:
                ProfileActivity.openActivity(this);
                break;
            case R.id.what_is_upgrade_btn:
                FreeVersionActivity.openActivity(this);
                break;

            case R.id.talk_to_expert:
                Utils.General.talkToExpert(this);
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onLockButtonClicked() {
        Intent intent = new Intent(this,FreeVersionActivity.class);
        startActivity(intent);
    }

    class MainFragmentPageAdapter extends FragmentPagerAdapter
    {
        HashMap<Integer,NameCompactFragment> idVsFragmentHashMap = new HashMap<>();

        public MainFragmentPageAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {

            NameCompactFragment fragment = null;
            switch (position)
            {
                case 0:
                    fragment = new HomeFragment();
                    break;

                case 1:
                    fragment= new ProgressFragment();
                    break;

                case 2:
                    List<Definition> definitionList = informationProvider.getDefinitionProvider().getDefinitionList();
                    List<Integer> definitionIds = new ArrayList<>();
                    for (Definition definition : definitionList) {
                        definitionIds.add(definition.getId());
                    }
                    fragment = DefinitionsFragment.getInstance(definitionIds);
                break;
            }

           // if(idVsFragmentHashMap.containsKey(position))
                idVsFragmentHashMap.put(position,fragment);

            return fragment;
        }

        @Override
        public int getCount() {
            return 3;
        }

        public NameCompactFragment getFragmentAt(int pos)
        {
            if(idVsFragmentHashMap.containsKey(pos))
               return idVsFragmentHashMap.get(pos);

            return null;
        }
    }
}
