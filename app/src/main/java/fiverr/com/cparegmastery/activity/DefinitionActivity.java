package fiverr.com.cparegmastery.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.List;

import fiverr.com.cparegmastery.R;
import fiverr.com.cparegmastery.fragment.definition.SingleDefinitionFragment;
import fiverr.com.cparegmastery.interfaces.InformationProvider;
import fiverr.com.cparegmastery.model.Definition;
import fiverr.com.cparegmastery.services.InformationService;
import fiverr.com.cparegmastery.utils.Utils;

public class DefinitionActivity extends AppCompatActivity implements SingleDefinitionFragment.OnFragmentInteractionListener,ViewPager.OnPageChangeListener {

    private List<Definition> definitionList;
    private ViewPager viewPager;
    private int currentPage;
    private ProgressBar progressBar;
    private TextView indicatorTV;

    private InformationProvider informationProvider;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        informationProvider = InformationService.getService();

        setContentView(R.layout.activity_definition);
        progressBar = (ProgressBar) findViewById(R.id.progress_bar);
        indicatorTV = (TextView) findViewById(R.id.no_indicator_tv);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        Intent intent = getIntent();
        int questionNo = intent.getIntExtra(getString(R.string.question_no_key),0);
        definitionList = informationProvider.getDefinitionProvider().getDefinitionList();

        progressBar.setMax(definitionList.size());

        DefinitionPageFragmentAdapter definitionPageFragmentAdapter = new DefinitionPageFragmentAdapter(getSupportFragmentManager());
        viewPager = (ViewPager) findViewById(R.id.viewPager);
        viewPager.setAdapter(definitionPageFragmentAdapter);
        viewPager.addOnPageChangeListener(this);
        viewPager.setCurrentItem(questionNo);
        onPageSelected(questionNo);
    }

    @Override
    public void onButtonClicked(SingleDefinitionFragment.ButtonType btnType) {

        if(currentPage!=viewPager.getCurrentItem())
            return;

        switch (btnType)
        {
            case PREVIOUS:
                   if(viewPager.getCurrentItem()>0) {
                       viewPager.setCurrentItem(viewPager.getCurrentItem() - 1, true);
                   }
                break;
            case NEXT:
                if(viewPager.getCurrentItem()<definitionList.size()-1)
                    viewPager.setCurrentItem(viewPager.getCurrentItem()+1,true);
                break;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.home_menu,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId())
        {
            case android.R.id.home:
                onBackPressed();
                break;

            case R.id.profile_btn:
                ProfileActivity.openActivity(this);
                break;

            case R.id.talk_to_expert:
                Utils.General.talkToExpert(this);
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    public ViewPager getViewPager() {
        return viewPager;
    }

    public List<Definition> getDefinitionList() {
        return definitionList;
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
        currentPage = position;
    }

    @Override
    public void onPageSelected(int position) {
        getSupportActionBar().setTitle(definitionList.get(position).getTitle());
        progressBar.setProgress(position+1);
        indicatorTV.setText( (position+1)+ " of "+definitionList.size());
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    class DefinitionPageFragmentAdapter extends FragmentStatePagerAdapter
    {

        public DefinitionPageFragmentAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {

            return SingleDefinitionFragment.newInstance(informationProvider.getDefinitionProvider().getDefinitionList().get(position),position);
        }

        @Override
        public int getCount() {
            return definitionList.size();
        }
    }
}
