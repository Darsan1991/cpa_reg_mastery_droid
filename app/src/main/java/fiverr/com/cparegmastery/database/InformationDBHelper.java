package fiverr.com.cparegmastery.database;

import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseErrorHandler;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

import fiverr.com.cparegmastery.model.Definition;
import fiverr.com.cparegmastery.model.Question;
import fiverr.com.cparegmastery.model.Section;
import fiverr.com.cparegmastery.model.Subject;

/**
 * Created by Darsan on 5/6/2017.
 */

public class InformationDBHelper extends SQLiteOpenHelper {

    private Context context;
    private SQLiteDatabase mDataBase;

    private String DB_PATH;
    private String DB_NAME = "Information.db";


    public InformationDBHelper(Context context) throws IOException {
        super(context, "Information", null, 1);
        this.context = context;
        createOrOpenDatabase();
    }

    public InformationDBHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) throws IOException {
        super(context, name, factory, version);
        this.context = context;
        createOrOpenDatabase();


    }

    public InformationDBHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version, DatabaseErrorHandler errorHandler) throws IOException {
        super(context, name, factory, version, errorHandler);
        this.context = context;

       createOrOpenDatabase();

    }

    private void createOrOpenDatabase() throws IOException {
        initDBPath();
        boolean dbexist = checkdatabase();
        if (dbexist) {
            System.out.println("Database exists");
            opendatabase();
        } else {
            System.out.println("Database doesn't exist");
            createdatabase();
            opendatabase();
        }
    }

    private void initDBPath()
    {
        DB_PATH = context.getFilesDir().getAbsolutePath();
    }


    public List<Subject> getSujects()
    {
       Cursor cursor = mDataBase.rawQuery("SELECT * FROM Subjects",null);


       int idIndex = cursor.getColumnIndex("Id");
       int titleIndex = cursor.getColumnIndex("Title");
        int premiumIndex = cursor.getColumnIndex("Premium");

        ArrayList<Subject> subjectArrayList = new ArrayList<>();

        if(cursor.moveToFirst())
        {
            do {
                int id = cursor.getInt(idIndex);
                boolean isPremium = cursor.getInt(premiumIndex) == 1;
                String title = cursor.getString(titleIndex);
                ArrayList<Integer> sectionIdList = new ArrayList<>();
                    String query = "SELECT * FROM Sections WHERE SubjectId="+id;
                    Cursor sectionCursor =  mDataBase.rawQuery(query,null);
                    int sectionIdIndex = sectionCursor.getColumnIndex("Id");
                    if(sectionCursor.moveToFirst()) {
                        do {
                            sectionIdList.add(sectionCursor.getInt(sectionIdIndex));
                        }while (sectionCursor.moveToNext());
                    }
                subjectArrayList.add(new Subject(id,title,sectionIdList,isPremium));
            }while (cursor.moveToNext());

        }

        return subjectArrayList;

    }

    public List<Section> getSections()
    {
        Cursor cursor = mDataBase.rawQuery("SELECT * FROM Sections",null);


        int idIndex = cursor.getColumnIndex("Id");
        int titleIndex = cursor.getColumnIndex("Title");
        int premiumIndex = cursor.getColumnIndex("Premium");
        int subjectIdIndex = cursor.getColumnIndex("SubjectId");

        ArrayList<Section> sectionArrayList = new ArrayList<>();

        if(cursor.moveToFirst())
        {
            do {
                int id = cursor.getInt(idIndex);
                boolean isPremium = cursor.getInt(premiumIndex) == 1;
                int subjectId = cursor.getInt(subjectIdIndex);
                String title = cursor.getString(titleIndex);
                ArrayList<Integer> questionIdList = new ArrayList<>();
                String query = "SELECT * FROM Questions WHERE SectionId="+id;
                Cursor questionCursor =  mDataBase.rawQuery(query,null);
                int questionIdIndex = questionCursor.getColumnIndex("Id");
                if(questionCursor.moveToFirst()) {
                    do {
                        questionIdList.add(questionCursor.getInt(questionIdIndex));
                    }while (questionCursor.moveToNext());
                }


                sectionArrayList.add(new Section(id,subjectId,title,questionIdList,isPremium));
            }while (cursor.moveToNext());

        }

        return sectionArrayList;
    }

    public List<Question> getQuestions()
    {
        Cursor cursor = mDataBase.rawQuery("SELECT * FROM Questions",null);


        int idIndex = cursor.getColumnIndex("Id");
        int answerIndexIndex = cursor.getColumnIndex("AnswerIndex");
        int optionsIndex = cursor.getColumnIndex("Options");
        int explanationIndex = cursor.getColumnIndex("Explanation");
        int imageUrlIndex = cursor.getColumnIndex("ImageUrl");
        int questionContextIndex = cursor.getColumnIndex("QuestionContext");
        int sectionIdIndex = cursor.getColumnIndex("SectionId");

        ArrayList<Question> questionArrayList = new ArrayList<>();
        if(cursor.moveToFirst())
        {
            do {
                String optionStr = cursor.getString(optionsIndex);
                String[] options = optionStr.split(",");

               Question question = new Question(
                        cursor.getInt(idIndex)
                        ,cursor.getString(questionContextIndex)
                        ,options
                        ,cursor.getInt(answerIndexIndex)
                        ,cursor.getInt(sectionIdIndex)
                       ,cursor.getString(explanationIndex)
                       ,cursor.getString(imageUrlIndex)
                );
                questionArrayList.add(question);

            }while (cursor.moveToNext());
        }


        return questionArrayList;
    }

    public List<Definition> getDefinitions()
    {
        Cursor cursor = mDataBase.rawQuery("SELECT * FROM Definitions",null);


        int idIndex = cursor.getColumnIndex("Id");
        int contextIndex = cursor.getColumnIndex("Context");
        int titleIndex = cursor.getColumnIndex("Title");


        ArrayList<Definition> definitionArrayList = new ArrayList<>();
        if(cursor.moveToFirst())
        {
            do {

                Definition definition = new Definition(
                       cursor.getInt(idIndex)
                        ,cursor.getString(titleIndex)
                        ,cursor.getString(contextIndex)
                );
                definitionArrayList.add(definition);

            }while (cursor.moveToNext());
        }


        return definitionArrayList;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }


    public void createdatabase() throws IOException {
        boolean dbexist = checkdatabase();
        if(dbexist) {
            System.out.println(" Database exists.");
        } else {
            this.getReadableDatabase();
            try {
                copydatabase();
            } catch(IOException e) {
                throw new Error("Error copying database");
            }
        }
    }

    private boolean checkdatabase() {

        boolean checkdb = false;
        try {
            String myPath = DB_PATH + DB_NAME;
            File dbfile = new File(myPath);
            checkdb = dbfile.exists();
        } catch(SQLiteException e) {
            System.out.println("Database doesn't exist");
        }
        return checkdb;
    }

    private void copydatabase() throws IOException {
        //Open your local db as the input stream
        InputStream myinput = context.getAssets().open(DB_NAME);

        // Path to the just created empty db
        String outfilename = DB_PATH + DB_NAME;

        //Open the empty db as the output stream
        OutputStream myoutput = new FileOutputStream(outfilename);

        // transfer byte to inputfile to outputfile
        byte[] buffer = new byte[1024];
        int length;
        while ((length = myinput.read(buffer))>0) {
            myoutput.write(buffer,0,length);
        }

        //Close the streams
        myoutput.flush();
        myoutput.close();
        myinput.close();
    }

    public void opendatabase() throws SQLException {
        //Open the database
        String mypath = DB_PATH + DB_NAME;
        mDataBase = SQLiteDatabase.openDatabase(mypath, null, SQLiteDatabase.OPEN_READWRITE);
    }

    public synchronized void close() {
        if(mDataBase != null) {
            mDataBase.close();
        }
        super.close();
    }

    public SQLiteDatabase getDataBase()
    {
        synchronized (this)
        {
            return mDataBase;
        }
    }

}
