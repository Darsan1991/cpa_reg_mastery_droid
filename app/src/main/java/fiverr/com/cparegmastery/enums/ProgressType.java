package fiverr.com.cparegmastery.enums;

public enum ProgressType
{
    UNANSWRED,DONT_KNOW,SOME_HOW_KNOW,KNOW
}
