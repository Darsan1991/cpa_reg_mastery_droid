package fiverr.com.cparegmastery.services;

import android.app.Service;

/**
 * Created by Darsan on 4/10/2017.
 */

public abstract class ManagableService extends Service {
    public abstract void stopService();
}
