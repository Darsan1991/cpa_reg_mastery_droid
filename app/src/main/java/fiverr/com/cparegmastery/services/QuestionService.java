package fiverr.com.cparegmastery.services;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.IBinder;
import android.support.annotation.Nullable;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import fiverr.com.cparegmastery.ApplicationManager;
import fiverr.com.cparegmastery.Presistance.FirebaseQuestionPresistance;
import fiverr.com.cparegmastery.QuestionProgressDto;
import fiverr.com.cparegmastery.enums.ProgressType;
import fiverr.com.cparegmastery.interfaces.InformationProvider;
import fiverr.com.cparegmastery.interfaces.OnTaskCompleteListner;
import fiverr.com.cparegmastery.interfaces.QuestionProgressPresistance;
import fiverr.com.cparegmastery.interfaces.QuestionProvider;
import fiverr.com.cparegmastery.interfaces.Refereshable;
import fiverr.com.cparegmastery.interfaces.TaskStatable;
import fiverr.com.cparegmastery.model.Question;
import fiverr.com.cparegmastery.model.QuestionProgress;

public class QuestionService extends ManagableService implements QuestionProvider,TaskStatable,Refereshable{

    private HashMap<Integer,QuestionProgress> questionProgressHashMap;
    private static QuestionService questionService;
    private static Intent intent;
    private State state;
    private InformationProvider informationProvider;
    private QuestionProgressPresistance questionProgressPresistance;

    public QuestionService() {
        questionService = this;
        questionProgressHashMap = new HashMap<>();
        informationProvider = InformationService.getService();
        questionProgressPresistance = new FirebaseQuestionPresistance();
        new InitAsyc().execute();

    }

    private class InitAsyc extends AsyncTask<Void,Void,Void>
    {
        OnTaskCompleteListner listner;
        public InitAsyc(OnTaskCompleteListner listner)
        {
            this.listner = listner;
        }

        public InitAsyc()
        {

        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            state = State.STARTED;
        }

        @Override
        protected Void doInBackground(Void... params) {
            while (((TaskStatable)informationProvider).getState() != State.FINISHED)
            {
                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }


            final List<Question> questions = informationProvider.getQuestionProvider().getAllQuestions();

            questionProgressPresistance.loadAllQuestions(new QuestionProgressPresistance.OnLoadAllQuestionsListner() {
                @Override
                public void onLoaded(List<QuestionProgressDto> questionProgressList) {
                    HashMap<Integer,QuestionProgressDto> progressDtoHashMap = new HashMap<>();
                    for (QuestionProgressDto questionProgressDto : questionProgressList) {
                        progressDtoHashMap.put(questionProgressDto.id,questionProgressDto);
                    }

                    for (Question question : questions) {

                        QuestionProgressDto questionProgressDto =progressDtoHashMap.containsKey(question.getId())?
                                progressDtoHashMap.get(question.getId())
                                :new QuestionProgressDto(question.getId(),false,0,false);
                        questionProgressHashMap.put(question.getId()
                                ,new QuestionProgress(question
                                        ,ProgressType.values()[questionProgressDto.progressType]
                                        ,questionProgressDto.isBookMarked
                                        ,questionProgressDto.isCorrect));
                    }

                    state = State.FINISHED;
                    if(listner!=null)
                        listner.onComplete(OnTaskCompleteListner.Result.SUCCESS);
                }
            });

            return null;

        }
    }


    public QuestionProgress getQuestionForID(int id)
    {
        if(questionProgressHashMap.containsKey(id))
            return questionProgressHashMap.get(id);

        return null;

    }

    public List<QuestionProgress> getQuestionProgressForIDs(List<Integer> ids)
    {
        List<QuestionProgress> questionProgresses = new ArrayList<>();
        for (Integer id : ids) {
            questionProgresses.add(getQuestionForID(id));
        }
        return questionProgresses;
    }

    public List<QuestionProgress> getAllQuestionProgress()
    {
        return new ArrayList<>(questionProgressHashMap.values());
    }

    public void setAsBookMark(int questionID,boolean isBookMarked)
    {
        if(!questionProgressHashMap.containsKey(questionID))
            return;
        QuestionProgress questionProgress = questionProgressHashMap.get(questionID);
        questionProgress.setBookMarked(isBookMarked);
        updatePresistance(questionProgress);
    }

    public void setAsCorrect(int questionID,boolean isCorrect)
    {
        if(!questionProgressHashMap.containsKey(questionID))
            return;
        QuestionProgress questionProgress = questionProgressHashMap.get(questionID);
        questionProgress.setAnswredCorrectly(isCorrect);
        updatePresistance(questionProgress);
    }

    private void updatePresistance(QuestionProgress questionProgress)
    {
        questionProgressPresistance.updateQuestionProgress(questionProgress.getId()
                , new QuestionProgressDto(questionProgress.getId(), questionProgress.isBookMarked(),questionProgress.getProgressType().ordinal(), questionProgress.isAnswredCorrectly())
                ,null
        );
    }

    @Override
    public void referesh(OnTaskCompleteListner listner) {
        state = State.UNKNOWN;
        questionProgressHashMap.clear();
        new InitAsyc(listner).execute();
    }

    public void setProgressType(int questionID, ProgressType progressType)
    {
        if(!questionProgressHashMap.containsKey(questionID))
            return;
        QuestionProgress questionProgress = questionProgressHashMap.get(questionID);
        questionProgress.setProgressType(progressType);

        updatePresistance(questionProgress);
    }

    @Override
    public State getState() {
        return state;
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void stopService() {
        stopSelf();
    }


    public static QuestionService getService()
    {
        if(questionService==null)
            startService(ApplicationManager.getInstance());
        return questionService;
    }

    public static QuestionService startService(Context context)
    {
        if(questionService!=null)
            return questionService;

        intent = new Intent(context,QuestionService.class);
        context.startService(intent);
        ApplicationManager.getInstance().setService(questionService, ApplicationManager.ServiceType.DEFINITION_SERVICE);
        return questionService;
    }


}
