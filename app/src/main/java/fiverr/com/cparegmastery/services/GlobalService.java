package fiverr.com.cparegmastery.services;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.IBinder;
import android.support.annotation.Nullable;

import fiverr.com.cparegmastery.AccountHandler;
import fiverr.com.cparegmastery.ApplicationManager;
import fiverr.com.cparegmastery.FirebaseAccountHandler;
import fiverr.com.cparegmastery.Presistance.FirebaseQuestionPresistance;
import fiverr.com.cparegmastery.activity.SplashActivity;
import fiverr.com.cparegmastery.interfaces.OnTaskCompleteListner;
import fiverr.com.cparegmastery.interfaces.QuestionProgressPresistance;
import fiverr.com.cparegmastery.interfaces.Refereshable;
import fiverr.com.cparegmastery.interfaces.TaskStatable;
import fiverr.com.cparegmastery.utils.Utils;

public class GlobalService extends ManagableService implements TaskStatable,Refereshable,QuestionProgressPresistance.OnSyncListner {
    private static final String TAG = GlobalService.class.getSimpleName();

    private static final String LAST_SYNCED = "LAST_SYNCED";
    private static final String SHARED_PREFERENCE = "PREFERENCE";
    private static GlobalService globalService;
    private static Intent intent;
    private boolean isPremiumVersion = false;
    private State state;
    private AccountHandler accountHandler;
    private QuestionProgressPresistance questionProgressPresistance;


    public GlobalService() {
        globalService = this;
        accountHandler = new FirebaseAccountHandler();
        questionProgressPresistance = new FirebaseQuestionPresistance();
        questionProgressPresistance.addOnStatusChangedListner(this);
        state = State.UNKNOWN;
        setService();

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        questionProgressPresistance.removeOnStatusChangedListner(this);
    }

    private void setService()
    {
        state = State.STARTED;
        accountHandler.isPremiumUnlocked(new AccountHandler.OnPremiumQueryListner() {
            @Override
            public void onSuccess(boolean isPremium) {
                setPremiumVersion(isPremium);
                state = State.FINISHED;
            }

            @Override
            public void onFailed() {
                state = State.FINISHED;
            }
        });

    }


    public boolean isPremiumVersion() {
        return isPremiumVersion;
    }

    private  void setPremiumVersion(boolean isPremiumVersion)
    {
        this.isPremiumVersion = isPremiumVersion;
    }

    private void setPremiumVersion(boolean isPremiumVersion, final OnTaskCompleteListner listner) {
        this.isPremiumVersion = isPremiumVersion;
        accountHandler.unlockPremiumLocked(true, new OnTaskCompleteListner() {
            @Override
            public void onComplete(Result result) {
                if(listner!=null)
                    listner.onComplete(result);
                Intent intent = new Intent(getApplicationContext(), SplashActivity.class);
                startActivity(intent);
            }
        });
    }

    public void unlockPremiumVersion(final OnTaskCompleteListner listner)
    {
        accountHandler.unlockPremiumLocked(true, new OnTaskCompleteListner() {
            @Override
            public void onComplete(Result result) {
                if(listner!=null)
                    listner.onComplete(result);

            }
        });
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void stopService() {
        stopSelf();
    }

    @Override
    public State getState() {
        return state;
    }

    @Override
    public void referesh(final OnTaskCompleteListner listner) {
        state = State.STARTED;
        accountHandler.isPremiumUnlocked(new AccountHandler.OnPremiumQueryListner() {
            @Override
            public void onSuccess(boolean isPremium) {
                setPremiumVersion(isPremium);
                state = State.FINISHED;
                if(listner!=null)
                {
                    listner.onComplete(OnTaskCompleteListner.Result.SUCCESS);
                }
            }

            @Override
            public void onFailed() {

                state = State.FINISHED;
                if(listner!=null)
                {
                    listner.onComplete(OnTaskCompleteListner.Result.FAILED);
                }
            }
        });


    }


    @Override
    public void onSync() {
//        Log.i(TAG,"onSync");
        SharedPreferences sharedPreferences = getSharedPreferences(SHARED_PREFERENCE, MODE_PRIVATE);

        if(Utils.NetworkUtils.hasInternetConnection(this)) {
            setCurrentTimeToLastSynced();
        }
        else if(!sharedPreferences.contains(LAST_SYNCED))
        {
            setCurrentTimeToLastSynced();
        }
    }

    private void setCurrentTimeToLastSynced()
    {
        SharedPreferences sharedPreferences = getSharedPreferences(SHARED_PREFERENCE, MODE_PRIVATE);
        SharedPreferences.Editor edit = sharedPreferences.edit();
        edit.putLong(LAST_SYNCED, System.currentTimeMillis());
        edit.apply();
    }

    public long lastSyncedTime()
    {
        SharedPreferences sharedPreferences  = getSharedPreferences(SHARED_PREFERENCE,MODE_PRIVATE);
        return sharedPreferences.getLong(LAST_SYNCED,0);
    }

    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    public static GlobalService getService()
    {
        if(globalService==null)
            startService(ApplicationManager.getInstance());
        return globalService;
    }

    public static GlobalService startService(Context context)
    {
        if(globalService!=null)
            return globalService;

        intent = new Intent(context,GlobalService.class);
        context.startService(intent);
        ApplicationManager instance = ApplicationManager.getInstance();
        instance.setService(globalService, ApplicationManager.ServiceType.GLOBAL_SERVICE);

        return (GlobalService)instance.getService(ApplicationManager.ServiceType.GLOBAL_SERVICE);
    }
}
