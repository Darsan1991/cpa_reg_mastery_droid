package fiverr.com.cparegmastery.services;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.IBinder;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import fiverr.com.cparegmastery.ApplicationManager;
import fiverr.com.cparegmastery.database.InformationDBHelper;
import fiverr.com.cparegmastery.interfaces.IPartialReferesher;
import fiverr.com.cparegmastery.interfaces.InformationProvider;
import fiverr.com.cparegmastery.interfaces.TaskStatable;
import fiverr.com.cparegmastery.model.Definition;
import fiverr.com.cparegmastery.model.Question;
import fiverr.com.cparegmastery.model.Section;
import fiverr.com.cparegmastery.model.Subject;

public class InformationService extends ManagableService implements InformationProvider,TaskStatable{

    private DefinitionService definitionService;
    private QuestionService questionService;
    private SubjectService subjectService;

    private static InformationService informationService;
    private  SectionService sectionService;
    private static Intent intent;
    private InformationDBHelper informationDBHelper;
    private State state;

    public InformationService() {
        informationService = this;
        initService();
    }

    private void initService()
    {
        state = State.UNKNOWN;
        new InitAsyc().execute();
    }

    private class InitAsyc extends AsyncTask<Void,Void,Void>
    {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            state = State.STARTED;
        }

        @Override
        protected Void doInBackground(Void... params) {
            try {
                informationDBHelper = new InformationDBHelper(ApplicationManager.getInstance());
                sectionService = new SectionService();
                definitionService = new DefinitionService();
                sectionService = new SectionService();
                questionService = new QuestionService();
                subjectService = new SubjectService();

                subjectService.lateReferesh();
                sectionService.lateReferesh();
                questionService.lateReferesh();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            state = State.FINISHED;
        }
    }

    public DefinitionProvider getDefinitionProvider() {
        return definitionService;
    }

    public QuestionProvider getQuestionProvider() {
        return questionService;
    }

    public SubjectProvider getSubjectProvider() {
        return subjectService;
    }

    public SectionProvider getSectionProvider() {
        return sectionService;
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public State getState() {
        return state;
    }

    public static InformationService getService()
    {
        if(informationService ==null)
             startService(ApplicationManager.getInstance());
        return informationService;
    }

    public static InformationService startService(Context context)
    {
        if(informationService !=null)
            return informationService;

        intent = new Intent(context,InformationService.class);
        context.startService(intent);
        ApplicationManager.getInstance().setService(informationService, ApplicationManager.ServiceType.DEFINITION_SERVICE);
        return informationService;
    }

    @Override
    public void stopService() {
        stopSelf();
    }

    public class SubjectService implements SubjectProvider,IPartialReferesher
    {
        private HashMap<Integer,Subject> subjectHashMap;
        private List<Subject> subjectArrayList;

        public SubjectService() {
            subjectArrayList = informationDBHelper.getSujects();
            subjectHashMap = new HashMap<>();

            for (Subject subject : subjectArrayList) {
                subjectHashMap.put(subject.getId(),subject);
            }
        }

        @Override
        public void lateReferesh() {

            for (Subject subject : subjectArrayList) {
                List<Section> sections = new ArrayList<>();
                for (Integer id : subject.getSectionIds()) {
                    sections.add(sectionService.getSectionForId(id));
                }
                subject.setSections(sections);
            }

        }

        public Subject getSubjectById(int id)
        {
            if(!subjectHashMap.containsKey(id))
                return null;
            return subjectHashMap.get(id);
        }

        public List<Subject> getAllSubjects()
        {
            return subjectArrayList;
        }
    }

    public class DefinitionService implements DefinitionProvider
    {
        private HashMap<Integer,Definition> idVsDefinitionHashMap;
        private List<Definition> definitionArrayList;

        public DefinitionService() {
            definitionArrayList = informationDBHelper.getDefinitions();
            idVsDefinitionHashMap = new HashMap<>();

            for (Definition definition:definitionArrayList) {
                idVsDefinitionHashMap.put(definition.getId(),definition);
            }
        }

        public Definition getDefinitionForNo(int questionNo)
        {
            return definitionArrayList.get(questionNo);
        }

        public List<Definition> getDefinitionsForIds(List<Integer> idList)
        {
            List<Definition> definitionList = new ArrayList<>();
            for (Integer id : idList) {
                definitionList.add(getDefinitionForId(id));
            }
            return definitionList;
        }

        public List<Definition> getDefinitionList()
        {
            return definitionArrayList;
        }

        public Definition getDefinitionForId(int id)
        {
            if(!idVsDefinitionHashMap.containsKey(id))
                return null;
            return idVsDefinitionHashMap.get(id);
        }
    }

    public class QuestionService implements QuestionProvider,IPartialReferesher
    {
        private HashMap<Integer,Question> questionHashMap;
        private List<Question> questionList;
        public QuestionService()
        {
            questionHashMap = new HashMap<>();
            questionList = informationDBHelper.getQuestions();

            for (Question question : questionList) {
               questionHashMap.put(question.getId(),question);
            }
        }

        @Override
        public void lateReferesh() {
            for (Question question : questionList) {
               Section section = sectionService.getSectionForId(question.getSectionID());
                question.setSection(section);
            }

        }

        public Question getQuestionById(int id)
        {
            if(!questionHashMap.containsKey(id))
                return null;

            return questionHashMap.get(id);
        }

        public List<Question> getAllQuestions()
        {
            return questionList;
        }

    }

    public class SectionService implements SectionProvider,IPartialReferesher
    {
        private HashMap<Integer,Section> idVsSectionHashMap;
        private List<Section> sectionArrayList;

        public SectionService() {
            sectionArrayList = informationDBHelper.getSections();
            idVsSectionHashMap = new HashMap<>();

            for (Section section:sectionArrayList) {
                idVsSectionHashMap.put(section.getId(),section);
            }
        }


        public List<Section> getSectionList()
        {
            return sectionArrayList;
        }

        public Section getSectionForId(int id)
        {
            if(!idVsSectionHashMap.containsKey(id))
                return null;
            return idVsSectionHashMap.get(id);
        }

        @Override
        public void lateReferesh() {
            for (Section section : sectionArrayList) {
                List<Question> questions = new ArrayList<>();
                for (Integer id : section.getRelatedQuestionIDs()) {
                    questions.add(questionService.getQuestionById(id));
                }
                section.setRelationQuestions(questions);
            }
        }
    }
}
