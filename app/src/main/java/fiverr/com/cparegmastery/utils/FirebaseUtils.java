package fiverr.com.cparegmastery.utils;

import android.support.annotation.NonNull;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ServerValue;

import fiverr.com.cparegmastery.interfaces.OnTaskCompleteListner;

/**
 * Created by Darsan on 5/14/2017.
 */

public class FirebaseUtils {

    public static DatabaseReference getUserDatabaseReference()
    {
        return FirebaseDatabase.getInstance()
                .getReference("users")
                .child(FirebaseAuth.getInstance().getCurrentUser().getUid());
    }

    private static DatabaseReference getDummyDatabaseReference()
    {
        return FirebaseDatabase.getInstance()
                .getReference("users")
                .child(FirebaseAuth.getInstance().getCurrentUser().getUid())
                .child("dummy");
    }

    public static DatabaseReference getQuestionProgressDatabaseReference()
    {
       return FirebaseDatabase.getInstance()
                .getReference("users")
                .child(FirebaseAuth.getInstance().getCurrentUser().getUid())
                .child("QuestionProgresses");
    }

    //Local and server database require write operation to sync
    public static void syncDatabase(final OnTaskCompleteListner taskCompleteListner)
    {
        FirebaseUtils.getUserDatabaseReference().keepSynced(true);
        FirebaseUtils.getDummyDatabaseReference().setValue(ServerValue.TIMESTAMP).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                FirebaseUtils.getUserDatabaseReference().keepSynced(false);
                if(taskCompleteListner != null)
                    taskCompleteListner.onComplete(task.isSuccessful()? OnTaskCompleteListner.Result.SUCCESS: OnTaskCompleteListner.Result.FAILED);
            }
        });
    }

    public static DatabaseReference getPremiumReference()
    {
        return FirebaseDatabase.getInstance()
                .getReference("users")
                .child(FirebaseAuth.getInstance().getCurrentUser().getUid())
                .child("Premium");
    }

    public static DatabaseReference getDisconnectReference()
    {
        return FirebaseDatabase.getInstance()
                .getReference("users")
                .child(FirebaseAuth.getInstance().getCurrentUser().getUid())
                .child("lastDisconnect");
    }
}
