package fiverr.com.cparegmastery.utils;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Environment;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ListView;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * Created by Darsan on 4/15/2017.
 */

public class Utils {

    public static class General
    {
        private static final String emailAddress= "darsan1991@gmail.com";
        public static String getEmailAddress()
        {
            return emailAddress;
        }
        public static Bitmap getScreenShot(View view) {
            View screenView = view.getRootView();
            screenView.setDrawingCacheEnabled(true);
            Bitmap bitmap = Bitmap.createBitmap(screenView.getDrawingCache());
            screenView.setDrawingCacheEnabled(false);
            return bitmap;
        }

        public static boolean hasPermissionToWrite(Context context)
        {
           return  (ContextCompat.checkSelfPermission(context,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED);
        }

        public static File saveBitmap(Bitmap bitmap,String name) {
            File imagePath = new File(Environment.getExternalStorageDirectory() + "/"+name);
            FileOutputStream fos;
            try {
                fos = new FileOutputStream(imagePath);
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, fos);
                fos.flush();
                fos.close();
            } catch (IOException e) {
                Log.e("GREC", e.getMessage(), e);
            }
            return imagePath;
        }

        public static Drawable getDrawableFromAssets(String name,Context context)
        {
            try {
                InputStream istr =context.getAssets().open(name);
                //set drawable from stream
               return Drawable.createFromStream(istr, null);
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        public static void composeEmail(Context context,String[] addresses, String subject,String body) {
            Intent intent = new Intent(Intent.ACTION_SENDTO);
            intent.setData(Uri.parse("mailto:")); // only email apps should handle this
            intent.putExtra(Intent.EXTRA_EMAIL, addresses);
            intent.putExtra(Intent.EXTRA_SUBJECT, subject);
            intent.putExtra(Intent.EXTRA_TEXT,body);
            context.startActivity(intent);
        }

        public static void talkToExpert(Context context)
        {
            composeEmail(context,new String[]{emailAddress},"CPA Mastery Help!","Write the help here...");
        }
    }

    public static class DpiUtils {

        public static int toPixels(int dp, DisplayMetrics metrics) {
            return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, metrics);
        }

    }

    public static class ListUtils {
        public static void setDynamicHeight(ListView mListView) {
            ListAdapter mListAdapter = mListView.getAdapter();
            if (mListAdapter == null) {
                // when adapter is null
                return;
            }
            int height = 0;
            int desiredWidth = View.MeasureSpec.makeMeasureSpec(mListView.getWidth(), View.MeasureSpec.UNSPECIFIED);
            for (int i = 0; i < mListAdapter.getCount(); i++) {
                View listItem = mListAdapter.getView(i, null, mListView);
                listItem.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED);
                height += listItem.getMeasuredHeight();
            }
            ViewGroup.LayoutParams params = mListView.getLayoutParams();
            params.height = height + (mListView.getDividerHeight() * (mListAdapter.getCount() - 1));
            mListView.setLayoutParams(params);
            mListView.requestLayout();
        }


    }

    public static class NetworkUtils
    {
        public static boolean hasInternetConnection(Context context)
        {
            ConnectivityManager ConnectionManager=(ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo networkInfo=ConnectionManager.getActiveNetworkInfo();
            return networkInfo != null && networkInfo.isConnected();
        }
    }

    public static class ButtonUtils
    {
        public static void setEnableButtonWithChangeAlpha(Button button,boolean enabled)
        {
            button.setEnabled(enabled);
            button.setAlpha(enabled?1:0.3f);
        }
    }

    public static class StringUtils
    {
        public static boolean isNullOrEmpty(String str)
        {
            return str == null || str.length()==0;
        }
    }

}
