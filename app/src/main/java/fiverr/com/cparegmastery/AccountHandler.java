package fiverr.com.cparegmastery;

import com.facebook.AccessToken;

import fiverr.com.cparegmastery.interfaces.OnTaskCompleteListner;

/**
 * Created by Darsan on 5/13/2017.
 */

public interface AccountHandler {
   void signUpWithFacebook(AccessToken accessToken,OnTaskCompleteListner listner);
   void signUpWithEmail(String emailAddress,String password,OnTaskCompleteListner listner);
   void signInWithEmail(String emailAddress,String password,OnTaskCompleteListner listner);
    void signOut();
    void resetPassword(OnTaskCompleteListner listner);
    String getEmailAddress();
    void updatePassword(String newPassword,OnTaskCompleteListner listner);
    void resetAccount(OnTaskCompleteListner listner);
    void isPremiumUnlocked(OnPremiumQueryListner onPremiumQueryListner);
    void unlockPremiumLocked(boolean isUnlock,OnTaskCompleteListner onTaskCompleteListner);
    interface OnTaskListner
    {
        void onResult(Result result);

        enum Result
        {
            SUCCESS,FAILED
        }
    }

    interface OnPremiumQueryListner
    {
        void onSuccess(boolean isPremium);
        void onFailed();
    }

}
