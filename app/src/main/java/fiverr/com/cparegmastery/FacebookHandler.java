package fiverr.com.cparegmastery;

import android.app.Activity;
import android.content.Intent;
import android.support.v4.app.Fragment;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;

import java.util.Arrays;

/**
 * Created by Darsan on 5/13/2017.
 */

public class FacebookHandler {

    private CallbackManager mCallbackManager;
    private OnLogInSucessListner onLogInSucessListner;


    public FacebookHandler() {
        init();
    }

    private void init()
    {
        mCallbackManager = CallbackManager.Factory.create();
        LoginManager.getInstance().registerCallback(mCallbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                onLogInSuccess();
            }

            @Override
            public void onCancel() {

            }

            @Override
            public void onError(FacebookException error) {

            }
        });
    }

    public boolean isLogIn()
    {
        return AccessToken.getCurrentAccessToken()!=null;
    }

    public AccessToken getCurrentAccessToken()
    {
        return AccessToken.getCurrentAccessToken();
    }

    private void onLogInSuccess()
    {
        if(onLogInSucessListner!=null)
            onLogInSucessListner.OnLoggedIn(AccessToken.getCurrentAccessToken());
    }

    public void logOut()
    {
        LoginManager.getInstance().logOut();
    }


    public void login(Activity activity,OnLogInSucessListner onLogInSucessListner)
    {
        this.onLogInSucessListner = onLogInSucessListner;
        if(AccessToken.getCurrentAccessToken()==null) {
            LoginManager.getInstance().logInWithReadPermissions(activity, Arrays.asList("email", "public_profile"));
        } else
        {
            onLogInSuccess();
        }
    }

    public void login(Fragment fragment, OnLogInSucessListner onLogInSucessListner)
    {
        this.onLogInSucessListner = onLogInSucessListner;
        if(AccessToken.getCurrentAccessToken()==null) {
                LoginManager.getInstance().logInWithReadPermissions(fragment, Arrays.asList("email", "public_profile"));
            }
        else
        {
            onLogInSuccess();
        }
    }

    public boolean onActivityResult(int requestCode, int resultCode, Intent data)
    {
       return mCallbackManager.onActivityResult(requestCode,resultCode,data);
    }

    public interface OnLogInSucessListner {
       void OnLoggedIn(AccessToken accessToken);
    }

    public interface OnLogOutFinishedListner
    {
        void onFinished();
    }
}
