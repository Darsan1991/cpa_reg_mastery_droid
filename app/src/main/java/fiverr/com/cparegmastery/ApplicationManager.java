package fiverr.com.cparegmastery;

import android.app.Application;
import android.app.Service;

import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;

import fiverr.com.cparegmastery.services.GlobalService;
import fiverr.com.cparegmastery.services.InformationService;
import fiverr.com.cparegmastery.services.ManagableService;
import fiverr.com.cparegmastery.services.QuestionService;

/**
 * Created by Darsan on 4/10/2017.
 */

public class ApplicationManager extends Application {

    private static ApplicationManager instance;

    private InformationService informationService;
    private QuestionService questionService;
    private GlobalService globalService;
    private ArrayList<ManagableService> appLifeTimeServiceList = new ArrayList<>();

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
        FirebaseDatabase.getInstance().setPersistenceEnabled(true);
        FirebaseDatabase.getInstance().goOffline();
        FirebaseDatabase.getInstance().goOnline();
        informationService = InformationService.startService(this);
    }

    @Override
    public void onTerminate() {
        for (ManagableService service: appLifeTimeServiceList) {
            service.stopService();
        }
        super.onTerminate();

    }

    public InformationService getInformationService() {
        if(informationService ==null)
            informationService = InformationService.startService(this);
        return informationService;
    }

    public QuestionService getQuestionService() {
        return questionService;
    }

    public void startService(ServiceType serviceType)
    {
        Service service = null;
        switch (serviceType)
        {
            case DEFINITION_SERVICE:
                service = InformationService.startService(this);
                break;

            case QUESTION_SERVICE:
                service = QuestionService.startService(this);
                break;
        }
        if(service instanceof ManagableService)
        setService((ManagableService) service,serviceType);
    }

    public Service getService(ServiceType serviceType)
    {
        switch (serviceType)
        {
            case DEFINITION_SERVICE:
                return informationService;
            case GLOBAL_SERVICE:
                return globalService;
            case QUESTION_SERVICE:
                return questionService;
        }
        return null;
    }

    public void setService(ManagableService service, ServiceType serviceType) {
        switch (serviceType)
        {
            case DEFINITION_SERVICE:
                if(informationService == service)
                    return;
                if(informationService !=null) {
                    service.stopService();
                    return;
                }

                try{
                     informationService = (InformationService) service;
                    appLifeTimeServiceList.add(informationService);
                }catch (Exception e)
                {
                    service.stopService();
                }
                break;

            case QUESTION_SERVICE:
                if(questionService == service)
                    return;
                if(questionService!=null) {
                    service.stopService();
                    return;
                }

                try{
                    questionService = (QuestionService) service;
                    appLifeTimeServiceList.add(informationService);
                }catch (Exception e)
                {
                    service.stopService();
                }
                break;

            case GLOBAL_SERVICE:
                if(globalService == service)
                    return;
                if(globalService!=null) {
                    service.stopService();
                    return;
                }

                try{
                    globalService = (GlobalService) service;
                    appLifeTimeServiceList.add(globalService);
                }catch (Exception e)
                {
                    service.stopService();
                }
                break;

        }
    }

    public static synchronized ApplicationManager getInstance() {
        return instance;
    }

    public enum ServiceType
    {
        DEFINITION_SERVICE,QUESTION_SERVICE,GLOBAL_SERVICE
    }
}
