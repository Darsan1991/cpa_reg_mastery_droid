package fiverr.com.cparegmastery.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import fiverr.com.cparegmastery.R;

/**
 * Created by Darsan on 4/13/2017.
 */

public class FilterTileView extends FrameLayout {

   private ImageView imageView,indicatorIV;
   private TextView textView;

    private int selectedBgColor,normalBgColor;
    private State currentState = State.NORMAL;
    private int selectedIndicatorId=R.drawable.indicators_selected,normalIndicatorId;

    public FilterTileView(Context context) {
        super(context);
        initView(context,null);
    }

    public FilterTileView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initView(context,attrs);
    }

    public FilterTileView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView(context,attrs);
    }

    public FilterTileView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        initView(context,attrs);
    }

    private void initView(Context context,AttributeSet attrs)
    {
        LayoutInflater inflater = LayoutInflater.from(context);
        inflater.inflate(R.layout.filter_tile_layout,this);
        if(attrs==null)
            return;
        TypedArray ta = context.obtainStyledAttributes(attrs, R.styleable.FilterTileView, 0, 0);
        try {
            normalBgColor = ta.getColor(R.styleable.FilterTileView_normal_background_color,getResources().getColor(R.color.colorAccent));
            selectedBgColor = ta.getColor(R.styleable.FilterTileView_selected_background_color,getResources().getColor(R.color.colorAccent));
            normalIndicatorId = ta.getResourceId(R.styleable.FilterTileView_src,R.drawable.indicators_selected);
            selectedIndicatorId = ta.getResourceId(R.styleable.FilterTileView_selected,R.drawable.indicators_selected);
            if(imageView!=null)
                imageView.setBackgroundColor(selectedBgColor);
        }finally {
            ta.recycle();
        }
    }

    public ImageView getImageView() {
        if(imageView==null)
            imageView = (ImageView) findViewById(R.id.imageView);
        return imageView;
    }

    public TextView getTextView() {
        if(textView==null)
            textView = (TextView) findViewById(R.id.question_count);
        return textView;
    }

    public void setText(String txt)
    {
        if(textView==null)
            textView = (TextView) findViewById(R.id.questionNo_tv);

       if(textView!=null)
           textView.setText(txt);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();

       textView = (TextView) this.findViewById(R.id.question_count);
        imageView = (ImageView)this.findViewById(R.id.imageView);
        indicatorIV = (ImageView)this.findViewById(R.id.indicator_iv);
        refereshUIs();
    }

    public void setSelected(boolean selected) {
        this.currentState = selected? State.SELECTED:State.NORMAL;
        refereshUIs();
    }

    private void refereshUIs()
    {
        imageView.setBackgroundColor(currentState==State.NORMAL?normalBgColor:selectedBgColor);
        indicatorIV.setImageResource(currentState==State.NORMAL?normalIndicatorId:selectedIndicatorId);
    }

    private enum  State
    {
        NORMAL,SELECTED
    }
}
