package fiverr.com.cparegmastery.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;

import fiverr.com.cparegmastery.R;

/**
 * Created by Darsan on 5/11/2017.
 */

public class CircluarProgressBar extends View {

    private int maxValue=100,currentProgress=90;
    private Paint backgroupPaint,fogroundPaint,textPaint;
    private int stokeWidth = 30;
    private int backGroundColor=200,forgroundColor=100,textSize=40;

    private String textStr;

    private int width;

    public CircluarProgressBar(Context context) {
        super(context);
        init(context,null);
    }

    public CircluarProgressBar(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init(context,attrs);
    }

    public CircluarProgressBar(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context,attrs);
    }

    public CircluarProgressBar(Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init(context,attrs);
    }

    private void init(Context context,AttributeSet attrs)
    {
        if(attrs!=null) {
            TypedArray ta = context.obtainStyledAttributes(attrs, R.styleable.CircluarProgressBar, 0, 0);
            try {
                backGroundColor = ta.getColor(R.styleable.CircluarProgressBar_background_color, getResources().getColor(R.color.colorAccent));
                forgroundColor = ta.getColor(R.styleable.CircluarProgressBar_forground_color, getResources().getColor(R.color.colorAccent));
                stokeWidth = ta.getInteger(R.styleable.CircluarProgressBar_stokeWidth, 22);
                maxValue = ta.getInteger(R.styleable.CircluarProgressBar_maxValue, 100);
                currentProgress = ta.getInteger(R.styleable.CircluarProgressBar_currentProgressValue, 0);
                textSize = (int) ta.getDimension(R.styleable.CircluarProgressBar_textSize,70);
            } finally {
                ta.recycle();
            }
        }

        textPaint = new Paint();
        textPaint.setColor(forgroundColor);
        textPaint.setTextSize(textSize);
        textPaint.setTextAlign(Paint.Align.CENTER);
        textPaint.setStyle(Paint.Style.FILL);

        backgroupPaint = new Paint();
        backgroupPaint.setColor(backGroundColor);
        backgroupPaint.setStyle(Paint.Style.STROKE);
        backgroupPaint.setStrokeWidth(stokeWidth);

        fogroundPaint = new Paint();
        fogroundPaint.setColor(forgroundColor);
        fogroundPaint.setStyle(Paint.Style.STROKE);
        fogroundPaint.setStrokeWidth(stokeWidth);
        fogroundPaint.setStrokeCap(Paint.Cap.ROUND);


    }

    public void setMaxValue(int maxValue) {
        this.maxValue = maxValue;
        invalidate();
    }

    public void setCurrentProgress(int currentProgress) {
        this.currentProgress = currentProgress;
        invalidate();
    }

    @Override
    protected void onDraw(Canvas canvas) {
        width = getWidth() < getHeight()?
                (getWidth()-getPaddingLeft()-getPaddingRight())
                :(getHeight()-getPaddingTop()-getPaddingBottom());
        textStr = (int)(100*currentProgress/(maxValue+0.0f))+"%";
        canvas.drawCircle(getWidth()/2,getHeight()/2,width/2-stokeWidth/2,backgroupPaint);
        canvas.drawArc(new RectF(
                        getWidth()/2 - width/2+stokeWidth/2
                        ,getHeight()/2-width/2+stokeWidth/2
                        , getWidth()/2 + width/2-stokeWidth/2
                        ,getHeight()/2+width/2 - stokeWidth/2
                )
                ,-90
                ,360*(currentProgress/(maxValue+0f)),false,fogroundPaint);

        Rect bounds = new Rect();
        textPaint.getTextBounds(textStr, 0, textStr.length(), bounds);
        int x = (getWidth() / 2) - (bounds.width() / 2);
        int y = (getHeight() / 2) - (bounds.height() / 2);


        canvas.drawText(textStr,getWidth()/2,getHeight()/2 - (textPaint.descent() + textPaint.ascent()) / 2,textPaint);
    }

}
