package fiverr.com.cparegmastery.view;


import android.animation.Animator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.DecelerateInterpolator;
import android.widget.LinearLayout;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import fiverr.com.cparegmastery.Adapter.ProgressAdapter;
import fiverr.com.cparegmastery.Adapter.QuestionsProgressAdapter;
import fiverr.com.cparegmastery.R;
import fiverr.com.cparegmastery.enums.ProgressType;

public class MultiProgressBar extends LinearLayout implements ProgressAdapter.ProgressAdapterListner, Animator.AnimatorListener {

    private static String TAG = MultiProgressBar.class.getSimpleName();
    private View dontKnowView,someHowKnowView,knowView,unAnswredView;
    private View  progressView;
    private HashMap<Integer,View> childHashmap = new HashMap<>();



    private List<Animator> currentAnimationList  = new ArrayList<>();
    private ProgressAdapter progressAdapter;
    private State currentState = State.MUlTIPLE;
    private ProgressType currentProgressType = ProgressType.DONT_KNOW;

    public MultiProgressBar(Context context) {
        super(context);
        initView(context);
    }

    public MultiProgressBar(Context context, AttributeSet attrs) {
        super(context, attrs);
        initView(context);
    }

    public MultiProgressBar(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView(context);
    }

    public MultiProgressBar(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        initView(context);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();

        progressView = getChildAt(0);
        //setBackground(getBackground());
        dontKnowView = findViewById(R.id.dont_know_filter);
        someHowKnowView = findViewById(R.id.some_how_know_filter);
        knowView = findViewById(R.id.know_filter);
        unAnswredView = findViewById(R.id.un_answred_filter);

        childHashmap.clear();
        childHashmap.put(dontKnowView.getId(),dontKnowView);
        childHashmap.put(someHowKnowView.getId(),someHowKnowView);
        childHashmap.put(knowView.getId(),knowView);
        childHashmap.put(unAnswredView.getId(),unAnswredView);
    }

    private void initView(Context context)
    {
        LayoutInflater.from(context).inflate(R.layout.multi_progress_bar,this);

    }

    public void showOnlyOneProgress(ProgressType progressType)
    {
        currentProgressType = progressType;
        currentState = State.SINGLE;
        int showProgressResourceID = 0;
        switch (progressType)
        {
            case UNANSWRED:
                showProgressResourceID = R.id.un_answred_filter;
                break;
            case DONT_KNOW:
                showProgressResourceID = R.id.dont_know_filter;
                break;
            case SOME_HOW_KNOW:
                showProgressResourceID = R.id.some_how_know_filter;
                break;
            case KNOW:
                showProgressResourceID = R.id.know_filter;
                break;

        }
        showOnlyProgress(showProgressResourceID);
    }

    public void reload()
    {
        resetProgressBar();
        onDataChanged();
    }

    private void resetProgressBar()
    {
        setWeightToView(dontKnowView,0);
        setWeightToView(someHowKnowView,0);
        setWeightToView(knowView,0);
        setWeightToView(unAnswredView,progressAdapter!=null?progressAdapter.getTotalQuestionCount():0);
    }

    private void animateView(final View view, float targetWeight)
    {

        int currentWeight = (int) getWeight(view);
        if(currentWeight == targetWeight)
            return;
        view.setVisibility(VISIBLE);
        view.setPivotX(0);
        ValueAnimator animation =  ValueAnimator.ofFloat(currentWeight,targetWeight);
        animation.setDuration(500);
        animation.setInterpolator(new DecelerateInterpolator());
        animation.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                setWeightToView(view,(float)valueAnimator.getAnimatedValue());
            }
        });
        animation.addListener(this);
        animation.start();
    }
    private void setWeightToView(View view,float weight)
    {
        LinearLayout.LayoutParams params = (LayoutParams) view.getLayoutParams();
        params.weight = weight;
        view.setLayoutParams(params);
    }

    private float getWeight(View view)
    {
        return ((LayoutParams)view.getLayoutParams()).weight;
    }

    public void setAdapter(ProgressAdapter progressAdapter,AnimateType animateType)
    {
        initAdapter(progressAdapter);
        currentState =  State.MUlTIPLE;
        if(animateType==AnimateType.BEGIN)
            resetProgressBar();
        showAllProgress();
    }

    public void setAdapterAndShowOneProgress(QuestionsProgressAdapter questionsProgressAdapter,ProgressType progressType,AnimateType animateType)
    {
        initAdapter(questionsProgressAdapter);
        currentState = State.SINGLE;
        if(animateType==AnimateType.BEGIN)
            resetProgressBar();

        showOnlyOneProgress(progressType);
    }

    private void initAdapter(ProgressAdapter progressAdapter)
    {
        if(this.progressAdapter!=null)
            this.progressAdapter.removeListners();

        this.progressAdapter = progressAdapter;
        this.progressAdapter.setListner(this);
    }

    public ProgressAdapter getAdapter()
    {
        return progressAdapter;
    }

    private float getWeightByID(int resourceID)
    {
        ProgressType progressType = ProgressType.DONT_KNOW;
        switch (resourceID)
        {
            case R.id.dont_know_filter:
                progressType = ProgressType.DONT_KNOW;
                break;

            case R.id.some_how_know_filter:
                progressType = ProgressType.SOME_HOW_KNOW;
                break;

            case R.id.know_filter:
                progressType = ProgressType.KNOW;
                break;

            case R.id.un_answred_filter:
                progressType = ProgressType.UNANSWRED;
                break;
        }
        return getWeightByProgressType(progressType);
    }

    private float getWeightByProgressType(ProgressType progressType)
    {
       return progressAdapter.getQuestionCount(progressType);
    }

    //set The Progress Resource id such as R.id.know_filter
    public void showOnlyProgress(int progressResourceID)
    {
        Iterator<Integer> iterator= childHashmap.keySet().iterator();
        while (iterator.hasNext())
        {
            int selected = iterator.next();
            View view = childHashmap.get(selected);
            if(view.getId()!=progressResourceID && unAnswredView.getId() != view.getId()) {
                animateView(view,0);
            }
            else if(view.getId()==progressResourceID){
                animateView(view,getWeightByID(progressResourceID));
            }
            else if(view.getId() == unAnswredView.getId())
            {
                animateView(unAnswredView,getUnSpecfiyQuestionsForState());
            }
        }

    }

    public void showAllProgress()
    {
        currentState = State.MUlTIPLE;
        Iterator<Integer> iterator= childHashmap.keySet().iterator();
        while (iterator.hasNext())
        {
            int val = iterator.next();
            float count = val!=R.id.un_answred_filter?getWeightByID(childHashmap.get(val).getId()):getUnSpecfiyQuestionsForState();
            animateView(childHashmap.get(val),count);
        }
    }

    private int getUnSpecfiyQuestionsForState()
    {
        float count = 0;

        if(currentState==State.MUlTIPLE) {
            Iterator<Integer> iterator = childHashmap.keySet().iterator();

            while (iterator.hasNext()) {
                int val = iterator.next();
                if (childHashmap.get(val).getId() != R.id.un_answred_filter)
                    count += getWeightByID(childHashmap.get(val).getId());
            }
        }
        else
        {
            count = getWeightByProgressType(currentProgressType);
        }

        return (int) (progressAdapter.getTotalQuestionCount() - count);
    }

    @Override
    public void onDataChanged() {
        if(currentState==State.SINGLE)
        {
            showOnlyOneProgress(currentProgressType);
        }
        else
        {
            showAllProgress();
        }
    }

    public void clearAnimation()
    {
        resetProgressBar();
        for (int i = 0; i < currentAnimationList.size(); i++) {
            currentAnimationList.get(i).cancel();
        }
        currentAnimationList.clear();
    }

    public void restartAnimation()
    {
        clearAnimation();
        resetProgressBar();
        if(currentState == State.SINGLE)
            showOnlyOneProgress(currentProgressType);
        else
            showAllProgress();
    }

    @Override
    public void onAnimationStart(Animator animation) {
        currentAnimationList.add(animation);
    }

    @Override
    public void onAnimationEnd(Animator animation) {
        currentAnimationList.remove(animation);
    }

    @Override
    public void onAnimationCancel(Animator animation) {
        currentAnimationList.remove(animation);
    }

    @Override
    public void onAnimationRepeat(Animator animation) {

    }


    public enum State
    {
        SINGLE,MUlTIPLE
    }

    public enum AnimateType
    {
        CONTINUE,BEGIN
    }

}
