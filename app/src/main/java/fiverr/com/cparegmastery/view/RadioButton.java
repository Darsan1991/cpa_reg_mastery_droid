package fiverr.com.cparegmastery.view;

import android.content.Context;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.ImageView;
import android.widget.LinearLayout;

import fiverr.com.cparegmastery.R;

/**
 * Created by Darsan on 4/21/2017.
 */

public class RadioButton extends LinearLayout {

    private ImageView imageView;
    private Type currentType = Type.SELECTION;

    private boolean isSelected;

    public RadioButton(Context context) {
        super(context);
        initView(context);
    }

    public RadioButton(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        initView(context);
    }

    public RadioButton(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView(context);
    }

    public RadioButton(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        initView(context);
    }

    private void initView(Context context)
    {
        LayoutInflater.from(context).inflate(R.layout.radio_button,this);

    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();

        imageView = (ImageView) findViewById(R.id.imageView);
        updateForType();

    }

    public Type getCurrentType() {
        return currentType;
    }

    public void setCurrentType(Type currentType) {
        this.currentType = currentType;
        updateForType();
    }
    private void updateForType()
    {
        int iconId = 0;
        switch (currentType)
        {
            case SELECTION:
                iconId = isSelected?R.drawable.ic_radio_button_checked:R.drawable.ic_radio_button_unchecked;
                break;

            case CORRECT:
                iconId = R.drawable.ic_correct;
                break;

            case WRONG:
                iconId = R.drawable.ic_wrong;
                break;
        }
        imageView.setImageDrawable(getResources().getDrawable(iconId));
        invalidate();
    }


    public boolean isSelectedRadio() {
        return isSelected;
    }


    public void setAsSelected(boolean selected) {

        if(currentType != Type.SELECTION)
            return;
        isSelected = selected;
       updateForType();
        invalidate();
    }

    public enum Type
    {
        SELECTION,CORRECT,WRONG
    }


}
