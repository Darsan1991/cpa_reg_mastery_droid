package fiverr.com.cparegmastery;

/**
 * Created by Darsan on 5/12/2017.
 */

public class QuestionProgressDto {
   public int id;
   public boolean isBookMarked;
   public int progressType;
   public boolean isCorrect;

    public QuestionProgressDto(int id, boolean isBookMarked, int progressType, boolean isCorrect) {
        this.id = id;
        this.isBookMarked = isBookMarked;
        this.progressType = progressType;
        this.isCorrect = isCorrect;
    }

}
