package fiverr.com.cparegmastery.model;

/**
 * Created by Darsan on 4/12/2017.
 */

public class Question {
    private int id;
    private String question;
    private String[] options;
    private int correctAnswerIndex;
    private String imageUrl;
    private String explanation;

    private int sectionID;
    private Section section;

    public Question(int id, String question, String[] options, int correctAnswerIndex, int section,String explanation,String imageUrl) {
        this.id = id;
        this.question = question;
        this.options = options;
        this.correctAnswerIndex = correctAnswerIndex;
        this.sectionID = section;
        this.explanation = explanation;
        this.imageUrl = imageUrl;
    }

    public Question(int id, String question, String[] options, int correctAnswerIndex, int sectionID,Section section,String explanation,String imageUrl) {
        this.id = id;
        this.question = question;
        this.options = options;
        this.correctAnswerIndex = correctAnswerIndex;
        this.sectionID = sectionID;
        this.section = section;
        this.explanation = explanation;
        this.imageUrl = imageUrl;
    }

    public int getId() {
        return id;
    }

    public String getQuestion() {
        return question;
    }

    public String[] getOptions() {
        return options;
    }

    public int getCorrectAnswerIndex() {
        return correctAnswerIndex;
    }

    public int getSectionID() {
        return sectionID;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public String getExplanation() {
        return explanation;
    }

    public Section getSection() {
        return section;
    }

    public void setSection(Section section) {
        this.section = section;
    }
}
