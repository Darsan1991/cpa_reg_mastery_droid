package fiverr.com.cparegmastery.model;

/**
 * Created by Darsan on 4/10/2017.
 */

public class Definition {
    private int id;
    private String title;
    private String context;

    public Definition(int id, String title, String context) {
        this.id = id;
        this.title = title;
        this.context = context;
    }

    public String getTitle() {
        return title;
    }

    public String getContext() {
        return context;
    }

    public int getId() {
        return id;
    }
}
