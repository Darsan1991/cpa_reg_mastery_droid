package fiverr.com.cparegmastery.model;
import fiverr.com.cparegmastery.enums.ProgressType;

/**
 * Created by Darsan on 4/18/2017.
 */

public class QuestionProgress extends Question {
    private ProgressType progressType;
    private boolean isBookMarked;
    private boolean isAnswredCorrectly;

//    public QuestionProgress(int id, String question, String[] answers, int correctAnswerIndex, Section section, ProgressType progressType,boolean isBookMarked) {
//        super(id, question, answers, correctAnswerIndex, section.getId());
//        this.progressType = progressType;
//        this.isBookMarked = isBookMarked;
//    }

    public QuestionProgress(Question question, ProgressType progressType, boolean isBookMarked, boolean isAnswredCorrectly)
    {
        super(question.getId(),question.getQuestion(),question.getOptions(),question.getCorrectAnswerIndex(),question.getSectionID(),question.getSection(),question.getExplanation(),question.getImageUrl());
        this.progressType = progressType;
        this.isBookMarked = isBookMarked;
        this.isAnswredCorrectly = isAnswredCorrectly;
    }

    public boolean isBookMarked() {
        return isBookMarked;
    }

    public void setBookMarked(boolean bookMarked) {
        isBookMarked = bookMarked;
    }

    public ProgressType getProgressType() {
        return progressType;
    }

    public void setProgressType(ProgressType progressType) {
        this.progressType = progressType;
    }

    public boolean isAnswredCorrectly() {
        return isAnswredCorrectly;
    }

    public void setAnswredCorrectly(boolean answredCorrectly) {
        isAnswredCorrectly = answredCorrectly;
    }
}


