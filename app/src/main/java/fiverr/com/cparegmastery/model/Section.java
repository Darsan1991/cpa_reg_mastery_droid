package fiverr.com.cparegmastery.model;

import java.util.List;

/**
 * Created by Darsan on 4/12/2017.
 */

public class Section {
    private int id;
    private String title;
    private int subjectId;
    private Subject subject;
    private List<Integer> relatedQuestionIDs;
    private List<Question> relationQuestions;
    private boolean isPremium;

    public Section(int id, int subjectId,String title, List<Integer> relatedQuestions,boolean isPremium) {
        this.id = id;
        this.subjectId = subjectId;
        this.title = title;
        this.relatedQuestionIDs = relatedQuestions;
        this.isPremium = isPremium;
    }

    public int getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public List<Integer> getRelatedQuestionIDs() {
        return relatedQuestionIDs;
    }

    public void setRelationQuestions(List<Question> relationQuestions) {
        this.relationQuestions = relationQuestions;
    }

    public boolean isPremium() {
        return isPremium;
    }

    public List<Question> getRelationQuestions() {
        return relationQuestions;
    }

    public int getSubjectId() {
        return subjectId;
    }

    public Subject getSubject() {
        return subject;
    }
}
