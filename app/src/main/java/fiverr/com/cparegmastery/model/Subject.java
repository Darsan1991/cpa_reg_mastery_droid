package fiverr.com.cparegmastery.model;

import java.util.List;

/**
 * Created by Darsan on 4/12/2017.
 */

public class Subject {
    private int id;
    private String title;
    private List<Integer> sectionIds;
    private List<Section> sections;
    private boolean isPremium;

    public Subject(int id, String title, List<Integer> sectionIds,boolean isPremium) {
        this.id = id;
        this.title = title;
        this.sectionIds = sectionIds;
        this.isPremium = isPremium;
    }

    public int getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public List<Integer> getSectionIds() {
        return sectionIds;
    }

    public List<Section> getSections() {
        return sections;
    }

    public void setSections(List<Section> sections) {
        this.sections = sections;
    }

    public boolean isPremium() {
        return isPremium;
    }
}
