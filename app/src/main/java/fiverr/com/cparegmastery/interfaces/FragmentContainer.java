package fiverr.com.cparegmastery.interfaces;

/**
 * Created by Darsan on 5/11/2017.
 */

public interface FragmentContainer {
    int getContainerId();
}
