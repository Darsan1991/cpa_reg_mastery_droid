package fiverr.com.cparegmastery.interfaces;

import java.util.List;

import fiverr.com.cparegmastery.QuestionProgressDto;

/**
 * Created by Darsan on 5/12/2017.
 */

public interface QuestionProgressPresistance{
    void addOnStatusChangedListner(OnSyncListner onSyncListner);
    void removeOnStatusChangedListner(OnSyncListner onSyncListner);
    void loadAllQuestions(OnLoadAllQuestionsListner listner);
    void updateQuestionProgress(int id, QuestionProgressDto progress, OnUpdateListner listner);
    void loadQuestionProgress(int id,OnLoadQuestionLisner listner);
    void reset(OnTaskCompleteListner listner);


    interface OnSyncListner
    {
        void onSync();
    }

    interface OnUpdateListner
    {
        void onUpdateFinished(QuestionProgressDto questionProgress);
    }

    interface OnLoadAllQuestionsListner
    {
        void onLoaded(List<QuestionProgressDto> questionProgressList);
    }

    interface OnLoadQuestionLisner
    {
        void onLoaded(QuestionProgressDto questionProgress);
    }

}
