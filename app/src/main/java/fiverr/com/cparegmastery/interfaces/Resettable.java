package fiverr.com.cparegmastery.interfaces;

/**
 * Created by Darsan on 5/13/2017.
 */

public interface Resettable {
    void reset(OnTaskCompleteListner listner);
}
