package fiverr.com.cparegmastery.interfaces;

import java.util.List;

import fiverr.com.cparegmastery.enums.ProgressType;
import fiverr.com.cparegmastery.model.QuestionProgress;

/**
 * Created by Darsan on 5/8/2017.
 */

public interface IProgressAdapter {
    void referesh();
    List<QuestionProgress> getQuestions(ProgressType progressType);
    List<QuestionProgress> getTotalQuestions();
    int getQuestionCount(ProgressType progressType);
    int getTotalQuestionCount();
    void setListner(IProgressListner listner);
    void removeListners();


    public interface IProgressListner
    {
        void onDataChanged();
    }
}
