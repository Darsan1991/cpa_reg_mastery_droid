package fiverr.com.cparegmastery.interfaces;

/**
 * Created by Darsan on 5/12/2017.
 */

public interface TaskStatable {

    State getState();

    enum State
    {
        FINISHED,UNKNOWN,STARTED
    }
}
