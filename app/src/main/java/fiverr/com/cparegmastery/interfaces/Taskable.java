package fiverr.com.cparegmastery.interfaces;

/**
 * Created by Darsan on 5/13/2017.
 */

public interface Taskable {
    void excute(OnTaskCompleteListner listner);

}
