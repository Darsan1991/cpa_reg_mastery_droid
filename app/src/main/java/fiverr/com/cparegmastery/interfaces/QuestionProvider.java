package fiverr.com.cparegmastery.interfaces;

import java.util.List;

import fiverr.com.cparegmastery.enums.ProgressType;
import fiverr.com.cparegmastery.model.QuestionProgress;

/**
 * Created by Darsan on 5/14/2017.
 */

public interface QuestionProvider {
    QuestionProgress getQuestionForID(int id);
    List<QuestionProgress> getQuestionProgressForIDs(List<Integer> ids);
    List<QuestionProgress> getAllQuestionProgress();
    void setAsBookMark(int questionID,boolean isBookMarked);
    void setAsCorrect(int questionID,boolean isCorrect);
    void setProgressType(int questionID, ProgressType progressType);
}
