package fiverr.com.cparegmastery.interfaces;

import java.util.List;

import fiverr.com.cparegmastery.model.Definition;
import fiverr.com.cparegmastery.model.Question;
import fiverr.com.cparegmastery.model.Section;
import fiverr.com.cparegmastery.model.Subject;

/**
 * Created by Darsan on 5/11/2017.
 */

public interface InformationProvider {

    DefinitionProvider getDefinitionProvider();
    QuestionProvider getQuestionProvider();
    SectionProvider getSectionProvider();
    SubjectProvider getSubjectProvider();

    interface DefinitionProvider
    {
       List<Definition> getDefinitionsForIds(List<Integer> idList);
        Definition getDefinitionForId(int id);
        List<Definition> getDefinitionList();
    }

    interface QuestionProvider
    {
        Question getQuestionById(int id);
        List<Question> getAllQuestions();
    }

    interface SectionProvider
    {
        List<Section> getSectionList();
        Section getSectionForId(int id);
    }

    interface SubjectProvider
    {
        Subject getSubjectById(int id);
        List<Subject> getAllSubjects();
    }
}
