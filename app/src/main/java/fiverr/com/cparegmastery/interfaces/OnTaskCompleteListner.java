package fiverr.com.cparegmastery.interfaces;

/**
 * Created by Darsan on 5/13/2017.
 */

public interface OnTaskCompleteListner {
    void onComplete(Result result);

    enum Result{
        SUCCESS,FAILED
    }
}
