package fiverr.com.cparegmastery.fragment.account;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import fiverr.com.cparegmastery.AccountHandler;
import fiverr.com.cparegmastery.FirebaseAccountHandler;
import fiverr.com.cparegmastery.R;
import fiverr.com.cparegmastery.interfaces.OnTaskCompleteListner;
import fiverr.com.cparegmastery.utils.Utils;


public class SignupWithEmailFragment extends Fragment implements View.OnClickListener {

    View mLaout;
    EditText emailET,passwordET,confirmPasswordET;

    ProgressDialog progressDialog;

    AccountHandler accountHandler;

    public SignupWithEmailFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        accountHandler = new FirebaseAccountHandler();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mLaout = inflater.inflate(R.layout.fragment_signup_with_email, container, false);
        emailET = (EditText) mLaout.findViewById(R.id.email_address);
        passwordET = (EditText) mLaout.findViewById(R.id.password);
        confirmPasswordET = (EditText) mLaout.findViewById(R.id.confirm_password);

        mLaout.findViewById(R.id.create_account).setOnClickListener(this);

        return mLaout;
    }

    @Override
    public void onClick(View v) {
        if(
                Utils.StringUtils.isNullOrEmpty(emailET.getText().toString())
                ||Utils.StringUtils.isNullOrEmpty(passwordET.getText().toString())
                ||Utils.StringUtils.isNullOrEmpty(confirmPasswordET.getText().toString())
                )
        {
            Toast.makeText(getContext(),"Field can't be null",Toast.LENGTH_LONG).show();
        }
        else
        {
           if(!passwordET.getText().toString().equals(confirmPasswordET.getText().toString()))
           {
               Toast.makeText(getContext(),"Password not match",Toast.LENGTH_LONG).show();
           }
           else
           {
               progressDialog = ProgressDialog.show(getContext(),"Sign Up","Waiting For Sign up");
                accountHandler.signUpWithEmail(emailET.getText().toString(), passwordET.getText().toString(), new OnTaskCompleteListner() {
                    @Override
                    public void onComplete(Result result) {
                        if(result == Result.SUCCESS)
                        {
                            getActivity().finish();
                        }
                        else
                        {
                            Toast.makeText(getContext(),"Error in SignUp.Please try againg",Toast.LENGTH_LONG).show();
                        }
                        progressDialog.dismiss();
                    }
                });
           }
        }
    }
}
