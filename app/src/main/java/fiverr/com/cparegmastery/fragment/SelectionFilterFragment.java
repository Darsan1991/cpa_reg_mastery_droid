package fiverr.com.cparegmastery.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import java.util.List;

import fiverr.com.cparegmastery.Adapter.BookMarkQuestionProgressAdapter;
import fiverr.com.cparegmastery.Adapter.QuestionsProgressAdapter;
import fiverr.com.cparegmastery.Adapter.list.SubjectSectionListAdapter;
import fiverr.com.cparegmastery.R;
import fiverr.com.cparegmastery.enums.ProgressType;
import fiverr.com.cparegmastery.model.QuestionProgress;
import fiverr.com.cparegmastery.utils.Utils;
import fiverr.com.cparegmastery.view.FilterTileView;


public class SelectionFilterFragment extends Fragment implements View.OnClickListener {

    private OnFilterFragmentListener mListener;

    private int selectedFilter = -1;
    private FilterTileView[] filterTileViews;
    private TextView titleTV;


    private float normalAlpha = 0.2f;
    private float selectedAlpha = 1f;

    private QuestionsProgressAdapter questionsProgressAdapter;
    private BookMarkQuestionProgressAdapter bookMarkedProgressAdapter;
    private List<QuestionProgress> selectedQuestionProgresses;
    private Button quickStartBtn;

    public SelectionFilterFragment() {
        // Required empty public constructor
    }




    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        filterTileViews = new FilterTileView[5];
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
       View layout = inflater.inflate(R.layout.fragment_selection_filter, container, false);


        for (int i=0;i<5;i++)
        {
            filterTileViews[i] = (FilterTileView) layout.findViewById(getResourceIDFromIndex(i));
            filterTileViews[i].setOnClickListener(this);
        }

        titleTV = (TextView) layout.findViewById(R.id.indicator_text);
        quickStartBtn = (Button) layout.findViewById(R.id.quick_start_btn);
        quickStartBtn.setOnClickListener(this);
        selectedFilter = selectedFilter < 0?0:selectedFilter;
        onClick(filterTileViews[selectedFilter]);
        return layout;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mListener = (OnFilterFragmentListener) getParentFragment();
    }

    private int getResourceIDFromIndex(int index)
    {
        switch (index)
        {
            case 0:
                return R.id.un_answred_filter;

            case 1:
                return R.id.dont_know_filter;

            case 2:
                return R.id.some_how_know_filter;

            case 3:
                return R.id.know_filter;

            case 4:
                return R.id.book_mark_filter;
        }
        return -1;
    }

    private String getTitleForFilter(int index)
    {
        switch (index)
        {
            case 0:
               return getString(R.string.un_answred_filter_title);

            case 1:
                return getString(R.string.dt_know_filter_title);

            case 2:
                return getString(R.string.some_how_know_filter_title);

            case 3:
                return getString(R.string.know_filter_title);

            case 4:
                return getString(R.string.booked_marked_filter_title);
        }
        return "";
    }

    @Override
    public void onClick(View view) {

        if(view.getId() == R.id.quick_start_btn)
        {
            if(mListener!=null)
                mListener.onQuickStartButtonClicked(selectedQuestionProgresses);
            return;
        }


        selectedFilter = selectedFilter<0?0:selectedFilter;
        filterTileViews[selectedFilter].setSelected(false);
        switch (view.getId())
        {
            case R.id.un_answred_filter:
                selectedFilter = 0;
                break;

            case R.id.dont_know_filter:
                selectedFilter = 1;
                break;

            case R.id.some_how_know_filter:
                selectedFilter = 2;
                break;

            case R.id.know_filter:
                selectedFilter = 3;
                break;

            case R.id.book_mark_filter:
                selectedFilter = 4;
                break;
        }
        filterTileViews[selectedFilter].setSelected(true);
        titleTV.setText(getTitleForFilter(selectedFilter));
        filterChanged(SubjectSectionListAdapter.Filter.values()[selectedFilter]);
    }


    private void filterChanged(SubjectSectionListAdapter.Filter filter)
    {
        if(filter != SubjectSectionListAdapter.Filter.BOOK_MARK)
        {
            if(questionsProgressAdapter!=null)
            selectedQuestionProgresses = questionsProgressAdapter.getQuestions(SubjectSectionListAdapter.filterToProgressType(filter));
        }
        else
        {
            if(bookMarkedProgressAdapter!=null)
                selectedQuestionProgresses = bookMarkedProgressAdapter.getBookMarkQuestionList();
        }


        if(selectedQuestionProgresses!=null
            && selectedQuestionProgresses.size()>0)
        {
            Utils.ButtonUtils.setEnableButtonWithChangeAlpha(quickStartBtn,true);
            quickStartBtn.setText("Quick Start - "+selectedQuestionProgresses.size()+" Question");
        }
        else
        {
            Utils.ButtonUtils.setEnableButtonWithChangeAlpha(quickStartBtn,false);
            quickStartBtn.setText("Quick Start - 0 Question");
        }



        if(mListener!=null)
            mListener.onFilterClicked(filter);
    }

    public void setQuestions(List<QuestionProgress> questionList)
    {
        questionsProgressAdapter = new QuestionsProgressAdapter(questionList);
        bookMarkedProgressAdapter = new BookMarkQuestionProgressAdapter(questionList);
        onClick(filterTileViews[selectedFilter]);
        refereshFliterViews();
    }

    public void referesh()
    {
        questionsProgressAdapter.referesh();
        bookMarkedProgressAdapter.referesh();
        refereshFliterViews();


    }

    private void refereshFliterViews()
    {
        if(filterTileViews != null)
        {
            filterTileViews[0].setText(questionsProgressAdapter.getQuestionCount(ProgressType.UNANSWRED)+"");
            filterTileViews[1].setText(questionsProgressAdapter.getQuestionCount(ProgressType.DONT_KNOW)+"");
            filterTileViews[2].setText(questionsProgressAdapter.getQuestionCount(ProgressType.SOME_HOW_KNOW)+"");
            filterTileViews[3].setText(questionsProgressAdapter.getQuestionCount(ProgressType.KNOW)+"");
            filterTileViews[4].setText(bookMarkedProgressAdapter.getBookMarkQuestionCount()+"");
        }
    }



    public interface OnFilterFragmentListener {

        void onFilterClicked(SubjectSectionListAdapter.Filter filter);
        void onQuickStartButtonClicked(List<QuestionProgress> questionProgresses);
    }
}
