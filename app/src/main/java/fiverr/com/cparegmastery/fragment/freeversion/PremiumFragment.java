package fiverr.com.cparegmastery.fragment.freeversion;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import fiverr.com.cparegmastery.R;
import fiverr.com.cparegmastery.activity.SplashActivity;
import fiverr.com.cparegmastery.interfaces.OnTaskCompleteListner;
import fiverr.com.cparegmastery.services.GlobalService;

public class PremiumFragment extends Fragment implements View.OnClickListener{

    private ProgressDialog progressDialog;
    private View mLayout;

    public PremiumFragment() {
        // Required empty public constructor
    }


    public static PremiumFragment newInstance(String param1, String param2) {
        PremiumFragment fragment = new PremiumFragment();
        return fragment;
    }

    public void onClick(View view)
    {
        progressDialog = ProgressDialog.show(getContext(),"UnLocking","waiting for unlock");
        GlobalService.getService().unlockPremiumVersion(new OnTaskCompleteListner() {
            @Override
            public void onComplete(Result result) {
                progressDialog.dismiss();
                Intent intent = new Intent(getActivity(), SplashActivity.class);
                startActivity(intent);
            }
        });
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mLayout = inflater.inflate(R.layout.fragment_premium, container, false);
        mLayout.findViewById(R.id.buy_btn).setOnClickListener(this);
        return mLayout;
    }


}
