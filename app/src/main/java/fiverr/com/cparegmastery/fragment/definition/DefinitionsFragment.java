package fiverr.com.cparegmastery.fragment.definition;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import fiverr.com.cparegmastery.Adapter.recycler.DefinitionRecyclerAdapter;
import fiverr.com.cparegmastery.R;
import fiverr.com.cparegmastery.fragment.NameCompactFragment;
import fiverr.com.cparegmastery.interfaces.InformationProvider;
import fiverr.com.cparegmastery.model.Definition;
import fiverr.com.cparegmastery.services.GlobalService;
import fiverr.com.cparegmastery.services.InformationService;


public class DefinitionsFragment extends NameCompactFragment {

    private static String SELECTED_DEFINITIONS = "selected_definitions";

    List<Definition> definitionArrayList;
    DefinitionRecyclerAdapter definitionRecyclerAdapter;
    private InformationProvider informationProvider;
    private GlobalService globalService;

    public DefinitionsFragment() {

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        globalService = GlobalService.getService();
        informationProvider = InformationService.getService();
        List<Integer> selectedDefinitionIds =getArguments().getIntegerArrayList(SELECTED_DEFINITIONS);
        definitionArrayList = informationProvider.getDefinitionProvider().getDefinitionsForIds(selectedDefinitionIds);
        definitionRecyclerAdapter = new DefinitionRecyclerAdapter(getActivity(), definitionArrayList);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        // Inflate the layout for this fragment
        View layout = inflater.inflate(R.layout.fragment_definitions, container, false);
        RecyclerView recyclerView = (RecyclerView) layout.findViewById(R.id.recycler_view);
        recyclerView.setAdapter(definitionRecyclerAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        return layout;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        inflater.inflate(R.menu.definitions_menu,menu);
        MenuItem menuItem = menu.findItem(R.id.search_btn);

        SearchView searchView = (SearchView) menuItem.getActionView();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                definitionRecyclerAdapter.appyFilter(s);
                return true;
            }
        });

        if(globalService.isPremiumVersion())
        menu.removeItem(R.id.what_is_upgrade_btn);

        super.onCreateOptionsMenu(menu, inflater);
    }

    public static DefinitionsFragment getInstance(List<Integer> defintionList)
    {
        Bundle bundle = new Bundle();
        bundle.putIntegerArrayList(SELECTED_DEFINITIONS,new ArrayList<>(defintionList));

        DefinitionsFragment definitionsFragment = new DefinitionsFragment();
        definitionsFragment.setArguments(bundle);

        return definitionsFragment;

    }

    @Override
    public String getTitle() {
        return "Definition";
    }
}
