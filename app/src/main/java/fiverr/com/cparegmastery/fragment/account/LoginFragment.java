package fiverr.com.cparegmastery.fragment.account;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import com.facebook.AccessToken;

import fiverr.com.cparegmastery.AccountHandler;
import fiverr.com.cparegmastery.FacebookHandler;
import fiverr.com.cparegmastery.FirebaseAccountHandler;
import fiverr.com.cparegmastery.R;
import fiverr.com.cparegmastery.interfaces.OnTaskCompleteListner;
import fiverr.com.cparegmastery.utils.Utils;

public class LoginFragment extends Fragment implements View.OnClickListener{

    private View mLayout;
    private EditText emailEF,passwordEF;

    private FacebookHandler facebookHandler;
    private AccountHandler accountHandler;

    private ProgressDialog progressDialog;

    public LoginFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        facebookHandler = new FacebookHandler();
        accountHandler = new FirebaseAccountHandler();
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mLayout = inflater.inflate(R.layout.fragment_login, container, false);
        emailEF = (EditText) mLayout.findViewById(R.id.email_address);
        passwordEF = (EditText) mLayout.findViewById(R.id.password);

        mLayout.findViewById(R.id.facebook_login_btn).setOnClickListener(this);
        mLayout.findViewById(R.id.login_with_email_btn).setOnClickListener(this);

        return mLayout;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(facebookHandler!=null)
            facebookHandler.onActivityResult(requestCode,resultCode,data);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.facebook_login_btn:
                facebookHandler.login(this,new FacebookHandler.OnLogInSucessListner() {
                    @Override
                    public void OnLoggedIn(AccessToken accessToken) {
                        showProgressDialog();
                        accountHandler.signUpWithFacebook(accessToken, new OnTaskCompleteListner() {
                            @Override
                            public void onComplete(Result result) {
                                hideProgreeDialog();
                                getActivity().finish();
                            }
                        }
                        );
                    }
                });
                break;

            case R.id.login_with_email_btn:
                if(Utils.StringUtils.isNullOrEmpty(emailEF.getText().toString())
                        ||Utils.StringUtils.isNullOrEmpty(passwordEF.getText().toString()))
                {
                    Toast.makeText(getContext(),"Fields cannot be empty",Toast.LENGTH_LONG);
                }
                else
                {
                    showProgressDialog();
                    accountHandler.signInWithEmail(
                            emailEF.getText().toString()
                            , passwordEF.getText().toString()
                            , new OnTaskCompleteListner() {
                                @Override
                                public void onComplete(Result result) {
                                    hideProgreeDialog();
                                    if(result == Result.SUCCESS)
                                    {
                                        Toast.makeText(getContext(),"Sucessfully Sing in!",Toast.LENGTH_LONG).show();
                                        getActivity().finish();
                                    }
                                    else
                                    {
                                        Toast.makeText(getContext(),"Sign in failed.Check the Details",Toast.LENGTH_LONG).show();
                                    }
                                }

                            }
                    );
                }
                break;
        }
    }

    private void showProgressDialog()
    {
        progressDialog = ProgressDialog
                .show(getContext(),"Sign In","Waiting for sign in");
    }

    private void hideProgreeDialog()
    {
        progressDialog.dismiss();
    }
}
