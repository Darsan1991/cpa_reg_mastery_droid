package fiverr.com.cparegmastery.fragment.quiz;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import fiverr.com.cparegmastery.R;
import fiverr.com.cparegmastery.extensions.FragmentStatePagerAdapterWithFragment;
import fiverr.com.cparegmastery.interfaces.QuestionProvider;
import fiverr.com.cparegmastery.model.Question;
import fiverr.com.cparegmastery.services.QuestionService;
import fiverr.com.cparegmastery.view.MultiProgressBar;


public class QuizFragment extends Fragment implements MultipleChoiseFragment.MultiChoiseQuizFragmentListner,ViewPager.OnPageChangeListener{

    private final static String QUESTIONS_KEY = "QUESTIONS";

    private ArrayList<Integer> quizList;
    private MultiProgressBar multiProgressBar;
    private TextView noIndicatorTV;

    private QuizPageFragmentAdapter quizPageFragmentAdapter;
    private ViewPager viewPager;
    private QuizFragmentListner listner;
    private QuestionProvider questionProvider;

    public QuizFragment() {

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        questionProvider = QuestionService.getService();
        quizList = getArguments().getIntegerArrayList(QUESTIONS_KEY);
        quizPageFragmentAdapter = new QuizPageFragmentAdapter(getChildFragmentManager());

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_quiz,container,false);
        viewPager = (ViewPager) view.findViewById(R.id.viewPager);
        viewPager.setAdapter(quizPageFragmentAdapter);
        viewPager.addOnPageChangeListener(this);
        onPageSelected(0);
        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if(context instanceof QuizFragmentListner)
        {
            listner = (QuizFragmentListner)context;
        }

    }

    @Override
    public void onDetach() {
        super.onDetach();
        listner = null;
    }


    class QuizPageFragmentAdapter extends FragmentStatePagerAdapterWithFragment<MultipleChoiseFragment>
    {

        public QuizPageFragmentAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {

            return MultipleChoiseFragment.newInstance(quizList.get(position),position);
        }

        @Override
        public int getCount() {
            return quizList.size();
        }
    }




    public void setQuestionIndex(int i)
    {
        viewPager.setCurrentItem(i);
    }

    public ArrayList<Integer> getQuizList() {
        return quizList;
    }

    public boolean goToNextIndex(boolean isAnimate)
    {
        if(viewPager.getCurrentItem() == quizList.size()-1)
            return false;
        setTheCurrentIndex(viewPager.getCurrentItem()+1,isAnimate);
        return true;
    }

    public boolean clearTheCurrentAndGoToNextIndex()
    {
        quizPageFragmentAdapter.getRegistredFragment(viewPager.getCurrentItem()).reset();
       return goToNextIndex(false);
    }

    public boolean hasNextQuiz()
    {
        return viewPager.getCurrentItem() < quizList.size()-1;
    }
    public void setTheCurrentIndex(int index,boolean isAnimate)
    {
        viewPager.setCurrentItem(index,isAnimate);
    }

    public int getCurrentIndex()
    {
        return viewPager.getCurrentItem();
    }

    public int getCurrentQuestionId()
    {
       return quizList.get(viewPager.getCurrentItem());
    }

    @Override
    public void onSubmitAnswerClicked(Question question, int selectedIndex) {

        if(listner!=null)listner.onSubmitAnswer(question,selectedIndex);
    }

    @Override
    public void onNextButtonClicked() {
        goToNextIndex(true);
    }

    @Override
    public void onPreviousbuttonClicked() {
        if(viewPager.getCurrentItem()==0)
            return;
        viewPager.setCurrentItem(viewPager.getCurrentItem()-1,true);
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        if(listner!=null)
            listner.onChangedQuestion(position);
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    public static QuizFragment getIntance(ArrayList<Integer> selectedQuestionProgress)
    {
        QuizFragment quizFragment = new QuizFragment();
        Bundle bundle = new Bundle();
        bundle.putIntegerArrayList(QUESTIONS_KEY,selectedQuestionProgress);

        quizFragment.setArguments(bundle);
        return quizFragment;
    }

    public interface QuizFragmentListner
    {
        void onSubmitAnswer(Question question,int selectedIndex);
        void onChangedQuestion(int index);
    }

}
