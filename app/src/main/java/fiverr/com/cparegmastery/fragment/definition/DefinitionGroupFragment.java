package fiverr.com.cparegmastery.fragment.definition;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

import fiverr.com.cparegmastery.Adapter.list.DefinitionGroupListAdapter;
import fiverr.com.cparegmastery.R;
import fiverr.com.cparegmastery.fragment.NameCompactFragment;
import fiverr.com.cparegmastery.interfaces.FragmentContainer;
import fiverr.com.cparegmastery.interfaces.InformationProvider;
import fiverr.com.cparegmastery.model.Definition;
import fiverr.com.cparegmastery.others.PremiumPopUp;
import fiverr.com.cparegmastery.services.GlobalService;
import fiverr.com.cparegmastery.services.InformationService;
import fiverr.com.cparegmastery.viewModel.DefinitionGroupTileUI;


public class DefinitionGroupFragment extends NameCompactFragment implements ListView.OnItemClickListener {

    DefinitionGroupListAdapter premiumDefinitionGroupListAdapter;
    private InformationProvider informationProvider;
    private GlobalService globalService;

    public DefinitionGroupFragment() {
        // Required empty public constructor
    }


    public static DefinitionGroupFragment newInstance() {
        DefinitionGroupFragment fragment = new DefinitionGroupFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        informationProvider = InformationService.getService();
        globalService = GlobalService.getService();
        DefinitionsGroup[] definitionsGroups = setupAndGetTheDefinitionGroups();
        List<DefinitionGroupTileUI> definitionGroupTileUIList = new ArrayList<>();

        for (DefinitionsGroup definitionsGroup : definitionsGroups) {
            definitionGroupTileUIList.add(new DefinitionGroupTileUI(definitionsGroup.firstLetter+" to "+definitionsGroup.lastLetter,definitionsGroup.getDefinitions()));
        }

        premiumDefinitionGroupListAdapter = new DefinitionGroupListAdapter(
        getActivity()
        , definitionGroupTileUIList
        ,!globalService.isPremiumVersion());

    }

    private DefinitionsGroup[] setupAndGetTheDefinitionGroups()
    {
        List<Definition> definitions = informationProvider.getDefinitionProvider().getDefinitionList();
        List<DefinitionsGroup> definitionGroupList = new ArrayList<>();
        //region:
        definitionGroupList.add(new DefinitionsGroup('A','C'));
        definitionGroupList.add(new DefinitionsGroup('D','F'));
        definitionGroupList.add(new DefinitionsGroup('G','L'));
        definitionGroupList.add(new DefinitionsGroup('M','O'));
        definitionGroupList.add(new DefinitionsGroup('P','R'));
        definitionGroupList.add(new DefinitionsGroup('S','Z'));

        for (Definition definition : definitions) {
            for (DefinitionsGroup definitionsGroup : definitionGroupList) {
                if(definitionsGroup.isTheLetterBelong(definition.getTitle().charAt(0)))
                {
                    definitionsGroup.addDefinition(definition);
                    break;
                }
            }
        }

        DefinitionsGroup[] definitionsGroups = new DefinitionsGroup[definitionGroupList.size()];
        definitionsGroups =  definitionGroupList.toArray(definitionsGroups);

        return definitionsGroups;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View mLayout = inflater.inflate(R.layout.fragment_definition_group, container, false);
        ListView listView = (ListView)mLayout.findViewById(R.id.list_view);
        listView.setAdapter(premiumDefinitionGroupListAdapter);
        listView.setOnItemClickListener(this);
        return mLayout;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

        if(premiumDefinitionGroupListAdapter.isLock())
        {
            (new  PremiumPopUp(getActivity())).show();

        }
        else
        {
            List<Definition> definitions = premiumDefinitionGroupListAdapter.getItem(position).getDefinitions();
            List<Integer> definitionIds = new ArrayList<>();

            for (Definition definition : definitions) {
                definitionIds.add(definition.getId());
            }

            DefinitionsFragment instance = DefinitionsFragment.getInstance(definitionIds);
            android.support.v4.app.FragmentManager fragmentManager = null;
            FragmentContainer fragmentContainer = null;
            if(getParentFragment()!=null && getParentFragment() instanceof FragmentContainer) {
                 fragmentContainer = (FragmentContainer) getParentFragment();
                fragmentManager = getParentFragment().getChildFragmentManager();
            }
            else if(getActivity() instanceof FragmentContainer)
            {
                fragmentContainer = (FragmentContainer) getActivity();
                fragmentManager = getActivity().getSupportFragmentManager();
            }

            if(fragmentContainer == null || fragmentManager==null)
                return;

            fragmentManager.beginTransaction().replace(fragmentContainer.getContainerId(), instance, "DefnitionsFragment").addToBackStack(null).commit();
            fragmentManager.executePendingTransactions();

        }
    }

    @Override
    public String getTitle() {
        return "Definitions Group";
    }

    private class DefinitionsGroup
    {
       public char firstLetter,lastLetter;
       private List<Definition> definitionList;

        public DefinitionsGroup(char firstLetter, char lastLetter) {
            this.firstLetter = firstLetter;
            this.lastLetter = lastLetter;
            definitionList = new ArrayList<>();
        }

        public boolean isTheLetterBelong(char letter)
        {
            char uppercaseLetter = letter;
            if(letter-'A'>26)
            {
                uppercaseLetter = (char)(letter - 'a' + 'A');
            }

            return uppercaseLetter>=firstLetter && uppercaseLetter <= lastLetter;
        }

        public void addDefinition(Definition definition)
        {
            definitionList.add(definition);
        }

        public List<Definition> getDefinitions()
        {
            return definitionList;
        }

    }


}
