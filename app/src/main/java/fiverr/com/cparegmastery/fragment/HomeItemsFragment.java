package fiverr.com.cparegmastery.fragment;

import android.animation.AnimatorInflater;
import android.animation.AnimatorSet;
import android.animation.ValueAnimator;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import java.util.ArrayList;
import java.util.List;

import fiverr.com.cparegmastery.Adapter.recycler.HomeRecyclerAdapter;
import fiverr.com.cparegmastery.R;
import fiverr.com.cparegmastery.activity.FreeVersionActivity;
import fiverr.com.cparegmastery.interfaces.InformationProvider;
import fiverr.com.cparegmastery.model.Subject;
import fiverr.com.cparegmastery.services.GlobalService;
import fiverr.com.cparegmastery.services.InformationService;
import fiverr.com.cparegmastery.utils.Utils;
import fiverr.com.cparegmastery.viewModel.HomeTileUI;

import static com.facebook.FacebookSdk.getApplicationContext;


public class HomeItemsFragment extends NameCompactFragment implements HomeRecyclerAdapter.OnClickItemListner,View.OnClickListener{

    ArrayList<HomeTileUI> homeTileUIArrayList;
    HomeRecyclerAdapter homeRecyclerAdapter;

    private OnHomeItemFragmentListner onHomeItemFragmentListner;
    private InformationProvider informationProvider;
    private GlobalService globalService;
    private View mLayout;

    public HomeItemsFragment() {


    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        informationProvider = InformationService.getService();
        globalService = GlobalService.getService();

        homeTileUIArrayList = new ArrayList<HomeTileUI>();
        List<Subject> subjects = informationProvider.getSubjectProvider().getAllSubjects();

        for (Subject subject : subjects) {
            homeTileUIArrayList.add(new HomeTileUI(subject.getTitle(),R.drawable.subject_icon, !globalService.isPremiumVersion()&& subject.isPremium(), HomeTileUI.Type.SUBJECT,subject));
        }

        homeTileUIArrayList.add(new HomeTileUI("Definitions",R.drawable.definition_icon,false, HomeTileUI.Type.DEFINITION));
        homeTileUIArrayList.add(new HomeTileUI("Progress",R.drawable.progress_icon,false, HomeTileUI.Type.PROGRESS));

        homeRecyclerAdapter = new HomeRecyclerAdapter(getActivity(),homeTileUIArrayList,this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mLayout = inflater.inflate(R.layout.fragment_home_items, container, false);
        RecyclerView recyclerView = (RecyclerView) mLayout.findViewById(R.id.recycler_view);
        recyclerView.setAdapter(homeRecyclerAdapter);
        //recyclerView.setLayoutManager(new GridLayoutManager(getActivity(),3));

        recyclerView.setLayoutManager(new GridLayoutManager(getActivity(),3));
        mLayout.findViewById(R.id.upgrade_btn).setOnClickListener(this);
        checkAndShowPremiumNotifaction();
        return mLayout;
    }

    private void checkAndShowPremiumNotifaction()
    {
        final View view = mLayout.findViewById(R.id.premium_notifaction);
        view.setVisibility(View.GONE);
        if(globalService.isPremiumVersion())
            return;
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(1200);
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if(!globalService.isPremiumVersion())
                            {
                                view.setVisibility(View.VISIBLE);
                                ValueAnimator valueAnimator = ValueAnimator.ofFloat(0,1);
                                valueAnimator.setDuration(1000);
                                valueAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                                    @Override
                                    public void onAnimationUpdate(ValueAnimator animation) {
                                        float value = (float) animation.getAnimatedValue();
                                        ViewGroup.LayoutParams layoutParams = view.getLayoutParams();
                                        layoutParams.height = Utils
                                                .DpiUtils
                                                .toPixels((int) (value*50),getResources().getDisplayMetrics());
                                        view.setLayoutParams(layoutParams);
                                    }
                                });

                                valueAnimator.start();
                            }

                        }
                    });
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        };

        new Thread(runnable).start();

    }

    public void onClick(View view)
    {
        switch (view.getId()) {
            case R.id.upgrade_btn:
            FreeVersionActivity.openActivity(getActivity());
                break;
        }
    }


    public void setOnHomeItemFragmentListner(OnHomeItemFragmentListner onHomeItemFragmentListner) {
        this.onHomeItemFragmentListner = onHomeItemFragmentListner;
    }

    @Override
    public void onItemClick(HomeTileUI homeTileUI) {
        if(onHomeItemFragmentListner!=null)
            onHomeItemFragmentListner.onClick(homeTileUI);
    }

    public interface OnHomeItemFragmentListner
   {
       public void onClick(HomeTileUI homeTileUI);
   }

    @Override
    public String getTitle() {
        return "CPA Mastery";
    }

}
