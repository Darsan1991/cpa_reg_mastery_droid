package fiverr.com.cparegmastery.fragment;

import android.animation.ValueAnimator;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.DecelerateInterpolator;
import android.widget.ListView;
import android.widget.ScrollView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import fiverr.com.cparegmastery.Adapter.BookMarkQuestionProgressAdapter;
import fiverr.com.cparegmastery.Adapter.QuestionsProgressAdapter;
import fiverr.com.cparegmastery.Adapter.list.ProgressListAdapter;
import fiverr.com.cparegmastery.Adapter.list.SubjectSectionListAdapter;
import fiverr.com.cparegmastery.R;
import fiverr.com.cparegmastery.enums.ProgressType;
import fiverr.com.cparegmastery.interfaces.InformationProvider;
import fiverr.com.cparegmastery.model.QuestionProgress;
import fiverr.com.cparegmastery.model.Section;
import fiverr.com.cparegmastery.services.GlobalService;
import fiverr.com.cparegmastery.services.InformationService;
import fiverr.com.cparegmastery.services.QuestionService;
import fiverr.com.cparegmastery.utils.Utils;
import fiverr.com.cparegmastery.view.CircluarProgressBar;
import fiverr.com.cparegmastery.viewModel.ProgressTileUI;



@RequiresApi(api = Build.VERSION_CODES.M)
public class ProgressFragment extends NameCompactFragment implements ScrollView.OnScrollChangeListener {

    public static String TAG = ProgressFragment.class.getSimpleName();

    private ProgressListAdapter progressListAdapter;
    private View mLayout;
    private QuestionService questionService;
    private InformationService informationService;
    private QuestionsProgressAdapter questionsProgressAdapter;
    private BookMarkQuestionProgressAdapter bookMarkQuestionProgressAdapter;

    private TextView unAnswredTV,dontKnowTV,someHowKnowTV,knowTV,bookMarkTV,resultTV;
    private ListView listView;
    private CircluarProgressBar circluarProgressBar;
    private InitAsync initAsync;
    private ListItemVisableListner listItemVisableListner;

    //for ListItemVisableListner
    private int maxOffsetCanbeAsVisable = 60;
    private int scrollViewHeight;
    private int currentScrollPosY = 0;
    private List<Integer> currentVisableList = new ArrayList<>();
    private InformationProvider informationProvider;
    private GlobalService globalService;

    public ProgressFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        informationProvider = InformationService.getService();
        globalService = GlobalService.getService();

        questionService = QuestionService.getService();
        informationService = InformationService.getService();
    }




    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        setHasOptionsMenu(true);
         mLayout = inflater.inflate(R.layout.fragment_progress, container, false);
        resultTV = (TextView) mLayout.findViewById(R.id.result_tv);

        unAnswredTV = (TextView) mLayout.findViewById(R.id.un_answred_tv);
        dontKnowTV = (TextView) mLayout.findViewById(R.id.dont_know_tv);
        someHowKnowTV = (TextView) mLayout.findViewById(R.id.some_how_know_tv);
        knowTV = (TextView) mLayout.findViewById(R.id.know_tv);
        bookMarkTV = (TextView) mLayout.findViewById(R.id.book_mark_tv);
        listView = (ListView) mLayout.findViewById(R.id.list_view);
        circluarProgressBar = (CircluarProgressBar) mLayout.findViewById(R.id.circular_progressBar);

        listView.setEnabled(false);

        final ScrollView scrollView = (ScrollView) mLayout.findViewById(R.id.scrollView);
        scrollView.setOnScrollChangeListener(this);

        scrollView.post(new Runnable() {
            @Override
            public void run() {
                scrollViewHeight = scrollView.getHeight();
                Log.i(TAG,"Scoroll Height:"+scrollViewHeight);
            }
        });

        return mLayout;

    }

    @Override
    public void onResume() {
        super.onResume();
        initAsync = new InitAsync();
        initAsync.execute();
    }

    @Override
    public void onPause() {
        super.onPause();
        if(initAsync!=null)
        initAsync.cancel(true);
    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        inflater.inflate(R.menu.progress_menu,menu);

        if(globalService.isPremiumVersion())
        {
            menu.removeItem(R.id.what_is_upgrade_btn);
        }
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public void onScrollChange(View v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
        currentScrollPosY = scrollY;
        List<Integer> visableListItems = getVisableListItems();

        List<Integer> becomeVisableItems = new ArrayList<>();
        List<Integer> becomeHideItems = new ArrayList<>();

        for (Integer visableListItem : visableListItems) {
            if(!currentVisableList.contains(visableListItem)) {
                becomeVisableItems.add(visableListItem);

            }
        }

        for (Integer pos : currentVisableList) {
            if(!visableListItems.contains(pos)) {
                becomeHideItems.add(pos);

            }
        }
        currentVisableList = visableListItems;

        if(becomeVisableItems.size() != 0)
        {
            if(listItemVisableListner!=null)listItemVisableListner.onItemBecomeVisable(becomeVisableItems);
        }

        if(becomeHideItems.size() != 0)
        {
            if(listItemVisableListner!=null)listItemVisableListner.onItemHide(becomeHideItems);
        }
    }

    public List<Integer> getVisableListItems()
    {
        if(listView.getChildCount()==0)
            return new ArrayList<>();
        List<Integer> positionList = new ArrayList<>();
        int relativeTopPos = listView.getTop() - currentScrollPosY;

        for (int i = 0; i < listView.getChildCount(); i++) {
            View v = listView.getChildAt(i);

            if((relativeTopPos  + v.getTop() > -maxOffsetCanbeAsVisable && relativeTopPos+v.getTop() < scrollViewHeight+maxOffsetCanbeAsVisable)
                ||(relativeTopPos  + v.getBottom() > -maxOffsetCanbeAsVisable && relativeTopPos+v.getBottom() < scrollViewHeight+maxOffsetCanbeAsVisable))
            {
                positionList.add(i);
            }

        }
        return positionList;

    }

    private void setValueToFilterAndAnimate(SubjectSectionListAdapter.Filter filter,int value)
    {
        TextView view = null;
        switch (filter)
        {
            case UNANSWRED:
                view =unAnswredTV;
                break;
            case DONT_KNOW:
                view = dontKnowTV;
                break;
            case SOME_HOW_KNOW:
                view = someHowKnowTV;
                break;
            case KNOW:
                view = knowTV;
                break;
            case BOOK_MARK:
                view = bookMarkTV;
                break;
        }
        ValueAnimator animation =  ValueAnimator.ofInt(0,value);
        animation.setDuration(500);
        animation.setInterpolator(new DecelerateInterpolator());
        final TextView finalView = view;
        animation.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                finalView.setText((int)valueAnimator.getAnimatedValue() + "");
            }
        });
        animation.start();
    }

    private  void setScoreAndAnimate(final int correct, final int answred)
    {
        final int percentage = (int) (100*correct/(answred+0f));
        ValueAnimator animation =  ValueAnimator.ofFloat(0,1);
        animation.setDuration(500);
        animation.setInterpolator(new DecelerateInterpolator());
        animation.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                float animatedValue = (float) valueAnimator.getAnimatedValue();
                int c = (int) (correct*animatedValue);
                int t = (int) (answred*animatedValue);

                if((float)valueAnimator.getAnimatedValue()>0.95) {
                    c = correct;
                    t = answred;
                }

                circluarProgressBar.setCurrentProgress((int) (animatedValue*percentage));
                resultTV.setText(c+"/"+t);
            }
        });
        animation.start();
    }

    @Override
    public String getTitle() {
        return "Progress";
    }

    private class InitAsync extends AsyncTask<Void,Void,Void>
    {
        List<ProgressTileUI> progressTileUIList;
        int totalAnswredQuestion;
        int correctionQuestion;

        @Override
        protected Void doInBackground(Void... params) {
            questionsProgressAdapter = new QuestionsProgressAdapter(questionService.getAllQuestionProgress());
            bookMarkQuestionProgressAdapter = new BookMarkQuestionProgressAdapter(questionService.getAllQuestionProgress());

            progressTileUIList = setAndGetProgressTileUIs();
            totalAnswredQuestion = getTotalAnswredQuestion();
            correctionQuestion = getCorrectAnswerQuestion();
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {

            //Animate Indicators
            setValueToFilterAndAnimate(SubjectSectionListAdapter.Filter.DONT_KNOW,questionsProgressAdapter.getQuestionCount(ProgressType.DONT_KNOW));
            setValueToFilterAndAnimate(SubjectSectionListAdapter.Filter.UNANSWRED,questionsProgressAdapter.getQuestionCount(ProgressType.UNANSWRED));
            setValueToFilterAndAnimate(SubjectSectionListAdapter.Filter.SOME_HOW_KNOW,questionsProgressAdapter.getQuestionCount(ProgressType.SOME_HOW_KNOW));
            setValueToFilterAndAnimate(SubjectSectionListAdapter.Filter.KNOW,questionsProgressAdapter.getQuestionCount(ProgressType.KNOW));
            setValueToFilterAndAnimate(SubjectSectionListAdapter.Filter.BOOK_MARK,bookMarkQuestionProgressAdapter.getBookMarkQuestionCount());


            setScoreAndAnimate(correctionQuestion,totalAnswredQuestion);
            progressListAdapter = new ProgressListAdapter(getContext(),progressTileUIList);
            listItemVisableListner = progressListAdapter;
            listView.setAdapter(progressListAdapter);
            Utils.ListUtils.setDynamicHeight(listView);


            listView.post(new Runnable() {
                @Override
                public void run() {
                    if(listItemVisableListner!=null) {
                        listItemVisableListner.onBecomeVisableAtStart(getVisableListItems());
                    }
                }
            });


        }

        private List<ProgressTileUI> setAndGetProgressTileUIs()
        {
            List<ProgressTileUI> progressTileUIs = new ArrayList<>();
            List<Section> sections = informationProvider.getSectionProvider().getSectionList();
            for (Section section : sections) {
                progressTileUIs.add(new ProgressTileUI(section.getTitle(), (int) getPercentageOfCorrectlyAnswred(section),new QuestionsProgressAdapter(questionService.getQuestionProgressForIDs(section.getRelatedQuestionIDs())),isAnyQuestionAnswred(section)));
            }

            return progressTileUIs;
        }

        private boolean isAnyQuestionAnswred(Section section)
        {
            List<QuestionProgress> questionProgresses = questionService.getQuestionProgressForIDs(section.getRelatedQuestionIDs());
            for (QuestionProgress questionProgress : questionProgresses) {
               if(questionProgress.getProgressType()!=ProgressType.UNANSWRED)
                   return true;
            }

            return false;
        }


        //This will return 0 if no one answred in Questions
        private float getPercentageOfCorrectlyAnswred(Section section)
        {
            int correctlyAnswred=0;
            int answredQuestion=0;

            List<QuestionProgress> questionProgresses = questionService.getQuestionProgressForIDs(section.getRelatedQuestionIDs());

            for (QuestionProgress questionProgress : questionProgresses) {
                if(questionProgress.getProgressType()!= ProgressType.UNANSWRED)
                {
                    answredQuestion++;
                    if(questionProgress.isAnswredCorrectly())
                        correctlyAnswred++;
                }
            }

            if(answredQuestion==0)
                return 0;

            return 100*correctlyAnswred/(answredQuestion+0.0f);

        }

        private int  getTotalAnswredQuestion()
        {
            int count = 0;
            for (ProgressType progressType : ProgressType.values()) {
                if(progressType!=ProgressType.UNANSWRED)
                    count+=questionsProgressAdapter.getQuestionCount(progressType);
            }
            return count;
        }

        private int getCorrectAnswerQuestion()
        {
            int count = 0;
            for (ProgressType progressType : ProgressType.values()) {
                if(progressType!=ProgressType.UNANSWRED) {
                    List<QuestionProgress> questions = questionsProgressAdapter.getQuestions(progressType);
                    for (QuestionProgress question : questions) {
                        if(question.isAnswredCorrectly())
                            count++;
                    }

                }
            }
            return count;
        }
    }

    public interface ListItemVisableListner
    {
        void onBecomeVisableAtStart(List<Integer> list);
        void onItemBecomeVisable(List<Integer> list);
        void onItemHide(List<Integer> list);
    }

}
