package fiverr.com.cparegmastery.fragment.definition;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import fiverr.com.cparegmastery.ApplicationManager;
import fiverr.com.cparegmastery.R;
import fiverr.com.cparegmastery.activity.DefinitionActivity;
import fiverr.com.cparegmastery.fragment.NameCompactFragment;
import fiverr.com.cparegmastery.interfaces.InformationProvider;
import fiverr.com.cparegmastery.model.Definition;
import fiverr.com.cparegmastery.services.InformationService;

public class SingleDefinitionFragment extends NameCompactFragment implements View.OnClickListener {

    Definition definition;
    int position;

    OnFragmentInteractionListener onFragmentInteractionListener;
    InformationProvider informationProvider;

    public SingleDefinitionFragment() {
        // Required empty public constructor
    }


    public static SingleDefinitionFragment newInstance(Definition definition,int position) {
        SingleDefinitionFragment fragment = new SingleDefinitionFragment();
        Bundle args = new Bundle();
        Context context = ApplicationManager.getInstance();
        args.putInt(context
                .getString(R.string.question_id_key)
                ,definition.getId()
        );
        args.putInt(context.getString(R.string.position_key),position);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {

        informationProvider = InformationService.getService();
        onFragmentInteractionListener = (OnFragmentInteractionListener) getActivity();

        super.onCreate(savedInstanceState);
        int defId;

        if(savedInstanceState!=null) {
            defId = savedInstanceState.getInt(getActivity().getString(R.string.question_id_key));
            position = savedInstanceState.getInt(getActivity().getString(R.string.position_key));
        }
        else
        {
           Bundle bundle = getArguments();
           defId  = bundle.getInt(getString(R.string.question_id_key));
            position = bundle.getInt(getString(R.string.position_key));
        }
        definition = informationProvider.getDefinitionProvider().getDefinitionForId(defId);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View layout = inflater.inflate(R.layout.fragment_single_definition, container, false);
        ((TextView)layout.findViewById(R.id.title_tv)).setText(definition.getTitle());
        ((TextView)layout.findViewById(R.id.context_tv)).setText(definition.getContext());

        View nextBtn = layout.findViewById(R.id.next_btn);
        View previousBtn = layout.findViewById(R.id.previous_btn);
        nextBtn.setOnClickListener(this);
        previousBtn.setOnClickListener(this);

        int totalChildCount = ((DefinitionActivity)getActivity()).getDefinitionList().size();


        nextBtn.setVisibility(position==totalChildCount-1?View.INVISIBLE:View.VISIBLE);
        previousBtn.setVisibility(position==0?View.INVISIBLE:View.VISIBLE);

        return layout;
    }




    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
          //  mListener = (OnFilterFragmentListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFilterFragmentListener");
        }
    }


    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(getActivity().getString(R.string.question_id_key),definition.getId());
        outState.putInt(getActivity().getString(R.string.position_key),position);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId())
        {
            case R.id.previous_btn:
                onFragmentInteractionListener.onButtonClicked(ButtonType.PREVIOUS);
                break;

            case R.id.next_btn:
                onFragmentInteractionListener.onButtonClicked(ButtonType.NEXT);
                break;
        }
    }

    public interface OnFragmentInteractionListener {
        void onButtonClicked(ButtonType btnType);
    }

    public enum ButtonType
    {
        NEXT,PREVIOUS
    }
}
