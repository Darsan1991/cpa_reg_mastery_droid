package fiverr.com.cparegmastery.fragment.account;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.facebook.AccessToken;

import fiverr.com.cparegmastery.AccountHandler;
import fiverr.com.cparegmastery.FacebookHandler;
import fiverr.com.cparegmastery.FirebaseAccountHandler;
import fiverr.com.cparegmastery.R;
import fiverr.com.cparegmastery.interfaces.FragmentContainer;
import fiverr.com.cparegmastery.interfaces.OnTaskCompleteListner;

public class SignupFragment extends Fragment implements View.OnClickListener{

    private View mLayout;
    private FacebookHandler facebookHandler;
    private AccountHandler accountHandler;

    public SignupFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        facebookHandler = new FacebookHandler();
        accountHandler = new FirebaseAccountHandler();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mLayout = inflater.inflate(R.layout.fragment_signup, container, false);
        mLayout.findViewById(R.id.facebook_login_btn).setOnClickListener(this);
        mLayout.findViewById(R.id.mail_login_btn).setOnClickListener(this);
        mLayout.findViewById(R.id.already_have_account).setOnClickListener(this);

        return mLayout;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.facebook_login_btn:
                    facebookHandler.login(this,new FacebookHandler.OnLogInSucessListner() {
                        @Override
                        public void OnLoggedIn(AccessToken accessToken) {
                            accountHandler.signUpWithFacebook(accessToken, new OnTaskCompleteListner() {
                                @Override
                                public void onComplete(Result result) {
                                    if(result == Result.SUCCESS)
                                        getActivity().finish();
                                }
                            });
                        }
                    });
                break;

            case R.id.mail_login_btn:
                    openFragment(new SignupWithEmailFragment());
                break;

            case R.id.already_have_account:
                openFragment(new LoginFragment());
                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(facebookHandler!=null)
            facebookHandler.onActivityResult(requestCode,resultCode,data);
    }

    private void openFragment(Fragment fragment)
    {
        if(getActivity() instanceof FragmentContainer) {
            FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                    fragmentManager
                    .beginTransaction()
                    .replace(((FragmentContainer)getActivity()).getContainerId(),fragment).addToBackStack(null).commit();
            fragmentManager.executePendingTransactions();
        }
    }
}
