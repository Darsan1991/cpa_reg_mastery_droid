package fiverr.com.cparegmastery.fragment;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import fiverr.com.cparegmastery.R;


public class HomeTabbarFragment extends Fragment implements ViewPager.OnPageChangeListener,View.OnClickListener {



    private ViewPager mViewPager;
    private View mHome,mProgress,mDefinition, unlock;
    private int selectedTab = -1;
    private float unSelectedAlpha = 0.3f;
    private boolean isShowUnlock;
    private HomeTabbarFragmentListner listner;

    public HomeTabbarFragment() {

    }

    public void showUnLockButton(boolean isShow)
    {
        isShowUnlock = isShow;
        if(unlock != null)
            unlock.setVisibility(isShow?View.VISIBLE:View.GONE);
    }

    public void hideUnLockButton()
    {
        isShowUnlock = false;
        if(unlock != null)
            unlock.setVisibility(View.GONE);
    }

    public void setViewPager(ViewPager viewPager)
    {
        mViewPager = viewPager;
        mViewPager.addOnPageChangeListener(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_home_tabbar, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Activity activity = getActivity();

        mHome = activity.findViewById(R.id.home);
        mProgress = activity.findViewById(R.id.progress);
        mDefinition = activity.findViewById(R.id.definion);
        unlock = activity.findViewById(R.id.unlock);

        unlock.setVisibility(isShowUnlock?View.VISIBLE:View.GONE);

        //set onItemClick
        mHome.setOnClickListener(this);
        mProgress.setOnClickListener(this);
        mDefinition.setOnClickListener(this);
        unlock.setOnClickListener(this);

        //set as UnSelected
        mHome.setAlpha(unSelectedAlpha);
        mProgress.setAlpha(unSelectedAlpha);
        mDefinition.setAlpha(unSelectedAlpha);

        onPageSelected(0);


    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        if(selectedTab!=position) {
            if(selectedTab>=0)
            getTabForPosId(selectedTab).setAlpha(unSelectedAlpha);
            selectedTab = position;
            getTabForPosId(selectedTab).setAlpha(1f);
        }


    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if(context instanceof HomeTabbarFragmentListner)
            listner = (HomeTabbarFragmentListner) context;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        listner = null;
    }

    private View getTabForPosId(int postion)
    {
        switch (postion)
        {
            case 0:
                return mHome;
            case 1:
                return mProgress;
            case 2:
                return mDefinition;
        }
        return null;
    }


    //On Tab Button Clicked
    @Override
    public void onClick(View view) {

        if(view.getId()==R.id.unlock)
        {
            if(listner!=null)
                listner.onLockButtonClicked();
            return;
        }


        int currentSelection = 0;
        switch (view.getId())
        {
            case R.id.home:
                currentSelection = 0;
                break;

            case R.id.progress:
                currentSelection = 1;
                break;

            case R.id.definion:
                currentSelection = 2;
                break;

        }

        if(selectedTab!=currentSelection)
            mViewPager.setCurrentItem(currentSelection,true);
    }

    public interface HomeTabbarFragmentListner
    {
        void onLockButtonClicked();
    }
}
