package fiverr.com.cparegmastery.fragment.quiz;

import android.animation.Animator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.media.Image;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.DecelerateInterpolator;
import android.widget.AbsListView;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Arrays;

import fiverr.com.cparegmastery.Adapter.list.MultiChoiceResultListAdapter;
import fiverr.com.cparegmastery.R;
import fiverr.com.cparegmastery.enums.ProgressType;
import fiverr.com.cparegmastery.model.Question;
import fiverr.com.cparegmastery.services.QuestionService;
import fiverr.com.cparegmastery.utils.Utils;


public class ResultFragment extends Fragment implements View.OnClickListener {
    private final static String lastStateKey = "LastState";
    private final static String QUESTION_ID = "Question";
    private static final String SELECTED_ANSWER = "SelectedAnswer";
    private static final String TAG = ResultFragment.class.getSimpleName();

    private OnFragmentInteractionListener mListener;
    private View view,showOrHideToolbar;
    private boolean isAnimating;
    private QuestionState currentQuestionState;
    private Question question;
    private int selectedAnswer;


    public ResultFragment() {
        // Required empty public constructor
    }



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle = getArguments();
       int questionID = bundle.getInt(QUESTION_ID);
        question = QuestionService.getService().getQuestionForID(questionID);
        selectedAnswer = bundle.getInt(SELECTED_ANSWER);
//        question = new Question(1,"IS this the Question?",new String[]{"True","False","Dont Know","All of the Above"},2,1);
//
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_result, container, false);
        this.view = view;

        view.findViewById(R.id.some_how_know_btn).setOnClickListener(this);
        view.findViewById(R.id.dont_know_btn).setOnClickListener(this);
        view.findViewById(R.id.know_btn).setOnClickListener(this);

        showOrHideToolbar = view.findViewById(R.id.viewOrHideQuestion_toolbar);
        showOrHideToolbar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onShowOrHideDefinitionButtonClicked(view);
            }
        });

        if(savedInstanceState == null)
        {
            currentQuestionState = QuestionState.HIDE;
        }
        else
        {
            currentQuestionState = QuestionState.values()[savedInstanceState.getInt(lastStateKey,0)];
        }


        TextView questionTV = (TextView) view.findViewById(R.id.question_txt);
        TextView explanationTV = (TextView) view.findViewById(R.id.explanation_txt);
        ImageView questionIV = (ImageView) view.findViewById(R.id.question_iv);

        questionTV.setText(question.getQuestion());
        explanationTV.setText(question.getExplanation());
        questionIV.setImageDrawable(Utils.General.getDrawableFromAssets(question.getImageUrl()+".jpg",getActivity()));
        calculateAndSetUpListForResult();
        setFrameLayoutHeightForStateAtStart(currentQuestionState);
        setShowOrHideToolbarForState(currentQuestionState);
//        ((ScrollView)((ViewGroup)view).getChildAt(0)).setScrollY(0);
        return view;
    }


    private void setFrameLayoutHeightForStateAtStart(final QuestionState questionState)
    {
        view.findViewById(R.id.result_content).post(new Runnable() {
            @Override
            public void run() {
                int height = view.findViewById(R.id.result_content).getHeight();
                ViewGroup.LayoutParams prarms = ((FrameLayout)view.findViewById(R.id.content_fragment)).getLayoutParams();
                prarms.height = height + (questionState==QuestionState.HIDE?0:view.findViewById(R.id.question_group).getHeight());
                view.findViewById(R.id.content_fragment).setLayoutParams(prarms);
            }
        });
    }

    private void setShowOrHideToolbarForState(QuestionState state)
    {
        View iconView = showOrHideToolbar.findViewById(R.id.toolbar_icon);
        iconView.setRotation(state==QuestionState.SHOWING?180:0);
        ((TextView)showOrHideToolbar.findViewById(R.id.toolbar_text)).setText(state==QuestionState.SHOWING?"Hide Question":"Show Question");
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {

        }
    }

    private void calculateAndSetUpListForResult()
    {
        int currentAnswer = selectedAnswer;
        boolean isCorrect = selectedAnswer == question.getCorrectAnswerIndex();

       final MultiChoiceResultListAdapter multiChoiceResultListAdapter = new MultiChoiceResultListAdapter(getActivity(),new ArrayList<>(Arrays.asList(question.getOptions())),question.getCorrectAnswerIndex(),selectedAnswer);
       final ListView listView = (ListView) view.findViewById(R.id.quiz_answer_list);
        listView.setAdapter(multiChoiceResultListAdapter);
        //listView.setSelected(true);
        listView.clearChoices();
        listView.setChoiceMode(AbsListView.CHOICE_MODE_SINGLE);

        listView.setItemChecked(selectedAnswer,true);
        listView.setEnabled(false);



        Utils.ListUtils.setDynamicHeight(listView);
        view.findViewById(R.id.correct_toolbar).setVisibility(isCorrect?View.VISIBLE:View.GONE);
        view.findViewById(R.id.wrong_toolbar).setVisibility(!isCorrect?View.VISIBLE:View.GONE);
    }

    public void onShowOrHideDefinitionButtonClicked(View view)
    {
        if(isAnimating)
            return;
        currentQuestionState = currentQuestionState == QuestionState.SHOWING?QuestionState.HIDE:QuestionState.SHOWING;
        setShowOrHideToolbarForState(currentQuestionState);
        showDefinition(currentQuestionState ==QuestionState.SHOWING);
    }


    private void showDefinition(boolean isShow)
    {
        if(isAnimating)
            return;

        isAnimating = true;
        final View content = view.findViewById(R.id.content_fragment);
        int contentHeight = view.findViewById(R.id.result_content).getHeight();
        int questionHeight = view.findViewById(R.id.question_group).getHeight();
        Log.i(TAG,"question Height:"+questionHeight);
        ValueAnimator animation =  ValueAnimator.ofFloat(isShow?contentHeight:contentHeight+questionHeight
                ,isShow?contentHeight+questionHeight:contentHeight
        );

//        getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LOCKED);
        animation.setDuration(1000);
        animation.setInterpolator(new DecelerateInterpolator());
        animation.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                ViewGroup.LayoutParams params = content.getLayoutParams();
                params.height = (int) (float)valueAnimator.getAnimatedValue();
                content.setLayoutParams(params);
            }
        });
        animation.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {

            }

            @Override
            public void onAnimationEnd(Animator animator) {
//                getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_FULL_SENSOR);
                isAnimating = false;
            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        });
        animation.start();
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(lastStateKey, currentQuestionState.ordinal());

    }

    @Override
    public void onClick(View view) {
        if(mListener==null || isAnimating)
            return;
        switch (view.getId())
        {
            case R.id.dont_know_btn:
                    mListener.onButtonClicked(ButtonType.DONT_KNOW);
                break;

            case R.id.some_how_know_btn:
                mListener.onButtonClicked(ButtonType.SOME_HOW_KNOW);
                break;

            case R.id.know_btn:
                mListener.onButtonClicked(ButtonType.KNOW);
                break;
        }
    }

    public static ResultFragment getInstance(int questionID,int selectedAnswer)
    {
        Bundle bundle = new Bundle();
        bundle.putInt(QUESTION_ID,questionID);
        bundle.putInt(SELECTED_ANSWER,selectedAnswer);

        ResultFragment resultFragment = new ResultFragment();
        resultFragment.setArguments(bundle);

        return resultFragment;
    }

    public static ProgressType buttonTypeToProgressType(ButtonType buttonType)
    {
        switch (buttonType)
        {
            case DONT_KNOW:
                return ProgressType.DONT_KNOW;
            case KNOW:
                return ProgressType.KNOW;
            case SOME_HOW_KNOW:
                return ProgressType.SOME_HOW_KNOW;
        }
        return ProgressType.UNANSWRED;
    }

    public interface OnFragmentInteractionListener {
        void onButtonClicked(ButtonType buttonType);

    }

    private enum QuestionState
    {
        HIDE,SHOWING
    }

    public enum ButtonType
    {
        DONT_KNOW,SOME_HOW_KNOW,KNOW
    }
}
