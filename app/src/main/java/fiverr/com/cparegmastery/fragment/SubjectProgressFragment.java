package fiverr.com.cparegmastery.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import fiverr.com.cparegmastery.Adapter.list.SubjectSectionListAdapter;
import fiverr.com.cparegmastery.R;
import fiverr.com.cparegmastery.activity.QuizActivity;
import fiverr.com.cparegmastery.interfaces.InformationProvider;
import fiverr.com.cparegmastery.model.QuestionProgress;
import fiverr.com.cparegmastery.model.Section;
import fiverr.com.cparegmastery.model.Subject;
import fiverr.com.cparegmastery.others.PremiumPopUp;
import fiverr.com.cparegmastery.services.GlobalService;
import fiverr.com.cparegmastery.services.InformationService;
import fiverr.com.cparegmastery.services.QuestionService;
import fiverr.com.cparegmastery.utils.Utils;
import fiverr.com.cparegmastery.viewModel.SectionTileUI;

public class SubjectProgressFragment extends Fragment implements SelectionFilterFragment.OnFilterFragmentListener,ListView.OnItemClickListener {

    public static final String SUBJECT_ID_KEY = "ID";

    private ListView avaliableSectionListView,premiumSectionListView;
    private View premiumGroupView;
    private SelectionFilterFragment selectionFilterFragment;

    SubjectSectionListAdapter avaliableSectionListAdapter,premiumSectionListAdapter;
    private InformationProvider informationProvider;
    private GlobalService globalService;

    public SubjectProgressFragment() {
    }



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        informationProvider = InformationService.getService();
        globalService = GlobalService.getService();

      int subjectId =  getArguments().getInt(SUBJECT_ID_KEY);
        Subject subject = informationProvider.getSubjectProvider().getSubjectById(subjectId);

        QuestionService questionService = QuestionService.getService();

        List<Section> sections = subject.getSections();

        ArrayList<SectionTileUI> freeSectionList = new ArrayList<>();
        ArrayList<SectionTileUI> premiumSectionList = new ArrayList<>();

        for (Section section : sections) {
            SectionTileUI sectionTileUI = new SectionTileUI(
                    section
                    ,questionService.getQuestionProgressForIDs(section.getRelatedQuestionIDs())
            );
            if(!section.isPremium() || globalService.isPremiumVersion())
            {
                freeSectionList.add(sectionTileUI);
            }
            else {
                premiumSectionList.add(sectionTileUI);
            }
        }


        avaliableSectionListAdapter = new SubjectSectionListAdapter(getContext(),freeSectionList,false);
        premiumSectionListAdapter = new SubjectSectionListAdapter(getContext(),premiumSectionList,true);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_subject_progress, container, false);

        premiumGroupView = view.findViewById(R.id.premium_group);
        avaliableSectionListView = (ListView) view.findViewById(R.id.avaliable_section_list_view);


        selectionFilterFragment = (SelectionFilterFragment) getChildFragmentManager().findFragmentById(R.id.selection_filter_fragment);
        selectionFilterFragment.setQuestions(getAvaliableQuestions());
        Utils.ButtonUtils.setEnableButtonWithChangeAlpha((Button)(view.findViewById(R.id.quick_start_btn)),true);
        avaliableSectionListView.setOnItemClickListener(this);

        avaliableSectionListView.setAdapter(avaliableSectionListAdapter);
        Utils.ListUtils.setDynamicHeight(avaliableSectionListView);


        premiumGroupView.setVisibility(globalService.isPremiumVersion()?View.GONE:View.VISIBLE);
        if(!globalService.isPremiumVersion()) {
            premiumSectionListView = (ListView) view.findViewById(R.id.premium_section_list_view);
            premiumSectionListView.setOnItemClickListener(this);
            premiumSectionListView.setAdapter(premiumSectionListAdapter);
            Utils.ListUtils.setDynamicHeight(premiumSectionListView);
        }

        return view;
    }

   private List<QuestionProgress> getAvaliableQuestions()
    {
        List<SectionTileUI> sections = avaliableSectionListAdapter.getSubjectSectionList();
        List<QuestionProgress> questionsList = new ArrayList<>();

        for (SectionTileUI section : sections) {
            questionsList.addAll(section.questionProgresses);
        }

        return questionsList;
    }

    @Override
    public void onResume() {
        super.onResume();
        avaliableSectionListAdapter.notifyDataSetChanged();
        selectionFilterFragment.referesh();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

    }

    @Override
    public void onFilterClicked(SubjectSectionListAdapter.Filter filter) {
        avaliableSectionListAdapter.setFilter(filter);
        premiumSectionListAdapter.setFilter(filter);
    }

    @Override
    public void onQuickStartButtonClicked(List<QuestionProgress> questionProgresses) {
        startTheQuizActivity(questionProgresses);
    }

    private void startTheQuizActivity(List<QuestionProgress> questionProgressList)
    {
        if(questionProgressList.size()==0) {
            Toast.makeText(getContext(),"There is no Question. Try Filters",Toast.LENGTH_LONG).show();
            return;
        }
        ArrayList<Integer> questionList = new ArrayList<>();
        for (QuestionProgress questionProgress : questionProgressList) {
            questionList.add(questionProgress.getId());
        }
        Intent intent = new Intent(getContext(), QuizActivity.class);
        intent.putIntegerArrayListExtra(QuizActivity.QUESTIONS_KEY,questionList);
        startActivity(intent);
    }

    public static SubjectProgressFragment getInstance(int id) {
        SubjectProgressFragment fragment = new SubjectProgressFragment();
        Bundle bundle = new Bundle();
        bundle.putInt(SUBJECT_ID_KEY,id);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
      if(parent.getAdapter() == avaliableSectionListAdapter) {
          startTheQuizActivity(avaliableSectionListAdapter.getQuestionsForfilter(position));
      }
      else if(parent.getAdapter() == premiumSectionListAdapter)
      {
         new PremiumPopUp(getContext()).show();
      }
    }
}
