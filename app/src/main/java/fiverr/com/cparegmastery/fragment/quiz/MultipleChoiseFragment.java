package fiverr.com.cparegmastery.fragment.quiz;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ScrollView;
import android.widget.TextView;

import java.util.Arrays;

import fiverr.com.cparegmastery.Adapter.list.MultiChoiseQuizListAdapter;
import fiverr.com.cparegmastery.ApplicationManager;
import fiverr.com.cparegmastery.R;
import fiverr.com.cparegmastery.activity.QuizActivity;
import fiverr.com.cparegmastery.model.Question;
import fiverr.com.cparegmastery.model.QuestionProgress;
import fiverr.com.cparegmastery.services.QuestionService;
import fiverr.com.cparegmastery.utils.Utils;


public class MultipleChoiseFragment extends Fragment implements View.OnClickListener,AdapterView.OnItemClickListener {

    private final static String QUESTION_KEY = "QUESTION";
    private final static String POSTION_KEY = "POSITION";

    private QuestionProgress question =null; //new QuestionProgress(new Question(1,"What is Your Name",new String[]{"Darsan","Hello","World","Java"},0,0)
//            , ProgressType.UNANSWRED,false);
    private int position;
    private MultiChoiseQuizListAdapter multiChoiseQuizListAdapter;
    private MultiChoiseQuizFragmentListner mListner;
    private View mLayout;

    private Button submitBtn;
    private int selectedAnswer=-1;


    public MultipleChoiseFragment() {
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Context context = ApplicationManager.getInstance();
        int questionID = 0;
        if(savedInstanceState==null)
        {
            Bundle bundle = getArguments();
             questionID = bundle.getInt(QUESTION_KEY);
            position = bundle.getInt(POSTION_KEY);
        }
        else
        {
             questionID = savedInstanceState.getInt(QUESTION_KEY);
             position = savedInstanceState.getInt(POSTION_KEY);
        }

        question = QuestionService.getService().getQuestionForID(questionID);

        multiChoiseQuizListAdapter = new MultiChoiseQuizListAdapter(context, Arrays.asList(question.getOptions()));

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mLayout = inflater.inflate(R.layout.fragment_multiple_choise,container,false);

        ListView listView = (ListView) mLayout.findViewById(R.id.quiz_answer_list);
        submitBtn = (Button) mLayout.findViewById(R.id.submit_answer_btn);
        View nextBtn = mLayout.findViewById(R.id.next_btn);
        View previousBtn = mLayout.findViewById(R.id.previous_btn);
        ImageView questionIV = (ImageView) mLayout.findViewById(R.id.question_iv);
        TextView questionTV = (TextView)mLayout.findViewById(R.id.question_txt);

        questionIV.setImageDrawable(
                Utils.General.getDrawableFromAssets(question.getImageUrl()+".jpg",getActivity())
//            getActivity().getDrawable(R.drawable.unlock_fill)
        );
        questionTV.setText(question.getQuestion());

        listView.setAdapter(multiChoiseQuizListAdapter);
        multiChoiseQuizListAdapter.setListView(listView);
        multiChoiseQuizListAdapter.setItemClickListener(this);

        Utils.ListUtils.setDynamicHeight(listView);


        int questionCount = (getActivity() instanceof QuizActivity)?((QuizActivity)getActivity()).getQuestionList().size():0;


        previousBtn.setVisibility(position!=0?View.VISIBLE:View.INVISIBLE);
        nextBtn.setVisibility(position!=questionCount-1?View.VISIBLE:View.INVISIBLE);

        previousBtn.setOnClickListener(this);
        nextBtn.setOnClickListener(this);
        submitBtn.setOnClickListener(this);


        Utils.ButtonUtils.setEnableButtonWithChangeAlpha(submitBtn,false);
//
        return mLayout;
    }



    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putInt(QUESTION_KEY,question.getId());
        outState.putInt(POSTION_KEY,position);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if(getParentFragment() instanceof MultiChoiseQuizFragmentListner)
        {
            mListner = (MultiChoiseQuizFragmentListner) getParentFragment();
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListner = null;
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        if(selectedAnswer==-1)
            Utils.ButtonUtils.setEnableButtonWithChangeAlpha(submitBtn,true);

        multiChoiseQuizListAdapter.setSelectedIndex(i);
        selectedAnswer = i;
    }

    public void onClick(View view) {
        switch (view.getId())
        {
            case R.id.next_btn:
                if(mListner!=null)mListner.onNextButtonClicked();
                break;

            case R.id.previous_btn:
                if(mListner!=null)mListner.onPreviousbuttonClicked();
                break;

            case R.id.submit_answer_btn:
                if(mListner!=null)mListner.onSubmitAnswerClicked(question,selectedAnswer);
                break;
        }
    }

    public static MultipleChoiseFragment newInstance(int questionID, int position) {
        MultipleChoiseFragment fragment = new MultipleChoiseFragment();
        Bundle args = new Bundle();
        args.putInt(QUESTION_KEY
                ,questionID
        );
        args.putInt(POSTION_KEY,position);
        fragment.setArguments(args);
        return fragment;
    }

    public void reset() {
        multiChoiseQuizListAdapter.clearSelection();
        ((ScrollView)((ViewGroup)mLayout).getChildAt(0)).setScrollY(0);
        Utils.ButtonUtils.setEnableButtonWithChangeAlpha(submitBtn,false);
        selectedAnswer = -1;
    }

    public interface MultiChoiseQuizFragmentListner
    {
        void onSubmitAnswerClicked(Question question,int selectedIndex);
        void onNextButtonClicked();
        void onPreviousbuttonClicked();
    }

}
