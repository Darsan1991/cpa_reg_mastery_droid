package fiverr.com.cparegmastery.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;

import fiverr.com.cparegmastery.R;
import fiverr.com.cparegmastery.activity.FreeVersionActivity;
import fiverr.com.cparegmastery.fragment.definition.DefinitionGroupFragment;
import fiverr.com.cparegmastery.interfaces.FragmentContainer;
import fiverr.com.cparegmastery.model.Subject;
import fiverr.com.cparegmastery.services.GlobalService;
import fiverr.com.cparegmastery.viewModel.HomeTileUI;


public class HomeFragment extends NameCompactFragment implements FragmentContainer, HomeItemsFragment.OnHomeItemFragmentListner {

    private GlobalService globalService;

    public HomeFragment() {

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        globalService = GlobalService.getService();
        HomeItemsFragment homeItemsFragment = new HomeItemsFragment();
        homeItemsFragment.setOnHomeItemFragmentListner(this);
        getChildFragmentManager().beginTransaction().add(R.id.home_frame, homeItemsFragment).commit();
        getChildFragmentManager().executePendingTransactions();
        getChildFragmentManager().addOnBackStackChangedListener((FragmentManager.OnBackStackChangedListener) getActivity());

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        View layout = inflater.inflate(R.layout.fragment_home, container, false);

        return layout;
    }
    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        inflater.inflate(R.menu.home_menu,menu);

        if(globalService.isPremiumVersion())
        {
            menu.removeItem(R.id.what_is_upgrade_btn);
        }

        super.onCreateOptionsMenu(menu, inflater);
    }




    @Override
    public void onClick(HomeTileUI homeTileUI) {
        Fragment fragment = null;
        switch (homeTileUI.getType())
        {
            case SUBJECT:
                if(homeTileUI.isLocked())
                {
                   Intent intent = new Intent(getContext(), FreeVersionActivity.class);
                    startActivity(intent);
                    return;
                }
                fragment = SubjectProgressFragment.getInstance(((Subject)homeTileUI.getExtraObject()).getId());

                break;


            case PROGRESS:
                 fragment = new ProgressFragment();
                break;

            case DEFINITION:
                fragment = new DefinitionGroupFragment();
                break;
        }

        if(fragment == null)
            return;

        getChildFragmentManager().beginTransaction().replace(getContainerId(),fragment).addToBackStack(null).commit();
        getChildFragmentManager().executePendingTransactions();
    }

    @Override
    public int getContainerId() {
        return R.id.home_frame;
    }
}
