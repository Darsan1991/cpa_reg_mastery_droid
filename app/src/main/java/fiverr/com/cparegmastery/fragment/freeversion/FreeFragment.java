package fiverr.com.cparegmastery.fragment.freeversion;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import fiverr.com.cparegmastery.R;


public class FreeFragment extends Fragment {

    public FreeFragment() {
        // Required empty public constructor
    }


    public static FreeFragment newInstance(String param1, String param2) {
        FreeFragment fragment = new FreeFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        return inflater.inflate(R.layout.fragment_free, container, false);
    }




}
