package fiverr.com.cparegmastery.Adapter.list;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

import fiverr.com.cparegmastery.R;
import fiverr.com.cparegmastery.viewModel.DefinitionGroupTileUI;

/**
 * Created by Darsan on 5/2/2017.
 */

public class DefinitionGroupListAdapter extends BaseAdapter{

    private Context context;
    private LayoutInflater inflater;

    private List<DefinitionGroupTileUI> definitionGroupTileUIList;
    private boolean isLock;


    public DefinitionGroupListAdapter(Context context, List<DefinitionGroupTileUI> definitionGroupTileUIList,boolean isLock) {
        this.context = context;
        this.definitionGroupTileUIList = definitionGroupTileUIList;
        inflater = LayoutInflater.from(context);
        this.isLock = isLock;
    }

    @Override
    public int getCount() {
        return definitionGroupTileUIList.size();
    }

    @Override
    public DefinitionGroupTileUI getItem(int position) {
        return definitionGroupTileUIList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return definitionGroupTileUIList.size();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if(convertView==null)
        {
            convertView = inflater.inflate(R.layout.definition_group_tile,parent,false);
        }
        TextView titleTV = (TextView) convertView.findViewById(R.id.title_tv);
        TextView definitionCountTV = (TextView) convertView.findViewById(R.id.definion_count_tv);
        View lockView = convertView.findViewById(R.id.lock_iv);

        DefinitionGroupTileUI definitionGroupTileUI = definitionGroupTileUIList.get(position);

        titleTV.setText(definitionGroupTileUI.getTitle());
        definitionCountTV.setText(definitionGroupTileUI.getDefinitions().size() + "");
        lockView.setVisibility(isLock?View.VISIBLE:View.INVISIBLE);

        return convertView;
    }

    public void setLock(boolean lock) {
        isLock = lock;
        notifyDataSetChanged();
    }

    public boolean isLock() {
        return isLock;
    }
}
