package fiverr.com.cparegmastery.Adapter.recycler;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import fiverr.com.cparegmastery.R;
import fiverr.com.cparegmastery.viewModel.HomeTileUI;

/**
 * Created by Darsan on 4/11/2017.
 */

public class HomeRecyclerAdapter extends RecyclerView.Adapter<HomeRecyclerAdapter.HomeViewHolder> {

    private List<HomeTileUI> homeTileUIList;
    private Context context;
    private OnClickItemListner onClickItemListner;

    public HomeRecyclerAdapter(Context context, List<HomeTileUI> homeTileUIList,OnClickItemListner onClickItemListner) {
        this.context = context;
        this.homeTileUIList = homeTileUIList;
        this.onClickItemListner = onClickItemListner;
    }

    @Override
    public HomeViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);

        return new HomeViewHolder(inflater.inflate(R.layout.home_recycler_tile,parent,false));
    }

    @Override
    public void onBindViewHolder(HomeViewHolder holder, int position) {
        holder.configureTile(homeTileUIList.get(position));
    }

    @Override
    public int getItemCount() {
        return homeTileUIList.size();
    }

    public interface OnClickItemListner
    {
        void onItemClick(HomeTileUI homeTileUI);
    }

    class HomeViewHolder extends RecyclerView.ViewHolder
    {
        private TextView titleTV;
        private ImageView iconIV;
        private ImageView lockIV;
        private HomeTileUI homeTileUI;

        public HomeViewHolder(View itemView) {
            super(itemView);
            titleTV = (TextView) itemView.findViewById(R.id.title_tv);
            iconIV = (ImageView) itemView.findViewById(R.id.icon_iv);
            lockIV = (ImageView) itemView.findViewById(R.id.lock_iv);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(onClickItemListner!=null)
                        onClickItemListner.onItemClick(homeTileUI);
                }
            });

        }

        public void configureTile(HomeTileUI homeTileUI)
        {
            this.homeTileUI = homeTileUI;
            titleTV.setText(homeTileUI.getTitle());
            iconIV.setImageResource(homeTileUI.getIconResourceID());
            lockIV.setVisibility(homeTileUI.isLocked()?View.VISIBLE:View.INVISIBLE);
        }
    }
}
