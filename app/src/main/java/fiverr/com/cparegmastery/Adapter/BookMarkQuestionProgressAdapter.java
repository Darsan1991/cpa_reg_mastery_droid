package fiverr.com.cparegmastery.Adapter;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import fiverr.com.cparegmastery.enums.ProgressType;
import fiverr.com.cparegmastery.model.QuestionProgress;

/**
 * Created by Darsan on 5/8/2017.
 */

public class BookMarkQuestionProgressAdapter extends ProgressAdapter{

    private List<QuestionProgress> bookMarkQuestionList;

    public BookMarkQuestionProgressAdapter(List<QuestionProgress> questionProgressList) {
       super(questionProgressList);

        for (ProgressType progressType : ProgressType.values()) {
            progressTypeArrayListHashMap.put(progressType,new ArrayList<QuestionProgress>());
        }
        bookMarkQuestionList = new ArrayList<>();
        referesh();
    }

    @Override
    public void referesh() {
        bookMarkQuestionList.clear();
        clearHashMap();

        for (QuestionProgress questionProgress : totalQuestionLists) {
            if(questionProgress.isBookMarked()) {
                bookMarkQuestionList.add(questionProgress);
                progressTypeArrayListHashMap.get(questionProgress.getProgressType()).add(questionProgress);
            }
        }
    }

    private void clearHashMap()
    {
        Iterator<ProgressType> iterator =  progressTypeArrayListHashMap.keySet().iterator();

        while (iterator.hasNext())
        {
            ProgressType progressType = iterator.next();
            progressTypeArrayListHashMap.get(progressType).clear();
        }
    }

    public List<QuestionProgress> getBookMarkQuestionList() {
        return bookMarkQuestionList;
    }

    public int getBookMarkQuestionCount()
    {
        return bookMarkQuestionList.size();
    }
}
