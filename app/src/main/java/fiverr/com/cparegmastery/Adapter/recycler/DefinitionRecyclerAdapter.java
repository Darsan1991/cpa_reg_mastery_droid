package fiverr.com.cparegmastery.Adapter.recycler;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import fiverr.com.cparegmastery.R;
import fiverr.com.cparegmastery.activity.DefinitionActivity;
import fiverr.com.cparegmastery.model.Definition;


public class DefinitionRecyclerAdapter extends RecyclerView.Adapter<DefinitionRecyclerAdapter.QuestionViewHolder> {

    private Context context;
    private LayoutInflater inflater;
    private List<Definition> activeList;
    private  List<Definition> totalList;


    public DefinitionRecyclerAdapter(Context context,List<Definition> definitionArrayList) {
        this.context = context;
        inflater = LayoutInflater.from(context);
        this.totalList = definitionArrayList;
        activeList = new ArrayList<>(totalList);
    }


    public void appyFilter(String filterTxt)
    {
        if(filterTxt==null || filterTxt.length()==0)
        {
            if(activeList.size()==totalList.size())
                return;
            activeList =new ArrayList<>(totalList);
            notifyDataSetChanged();
            return;
        }
        activeList.clear();
        for (Definition definition : totalList) {
            if(definition.getTitle().toLowerCase().startsWith(filterTxt.toLowerCase()))
            {
                activeList.add(definition);
            }
        }
        notifyDataSetChanged();
    }

    @Override
    public QuestionViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new QuestionViewHolder(inflater.inflate(R.layout.definition_recycler_tile,parent,false));
    }

    @Override
    public void onBindViewHolder(QuestionViewHolder holder, int position) {
        holder.configureTile(activeList.get(position),position);
    }

    @Override
    public int getItemCount() {
        return activeList.size();
    }

    class QuestionViewHolder extends RecyclerView.ViewHolder
    {
       private TextView textView;
        Definition definition;
        int postion;

        public QuestionViewHolder(View itemView) {
            super(itemView);
            textView = (TextView) itemView.findViewById(R.id.title);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(context, DefinitionActivity.class);
                    intent.putExtra(context.getString(R.string.question_no_key),postion);
                    context.startActivity(intent);
                }
            });
        }

        public void configureTile(Definition definition,int postion)
        {
            textView.setText(definition.getTitle());
            this.definition = definition;
            this.postion = postion;
        }

    }


}
