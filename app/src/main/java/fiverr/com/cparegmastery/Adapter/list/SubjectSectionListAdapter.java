package fiverr.com.cparegmastery.Adapter.list;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import fiverr.com.cparegmastery.Adapter.BookMarkQuestionProgressAdapter;
import fiverr.com.cparegmastery.Adapter.QuestionsProgressAdapter;
import fiverr.com.cparegmastery.R;
import fiverr.com.cparegmastery.enums.ProgressType;
import fiverr.com.cparegmastery.model.QuestionProgress;
import fiverr.com.cparegmastery.services.QuestionService;
import fiverr.com.cparegmastery.view.MultiProgressBar;
import fiverr.com.cparegmastery.viewModel.SectionTileUI;


public class SubjectSectionListAdapter extends BaseAdapter{

   private List<SectionTileUI> subjectSectionList;
    boolean isLocked;
    Context context;
    LayoutInflater inflater;
    List<AdapterGroup> adapterGroupList;

    Filter currentFilter = Filter.UNANSWRED;

    public SubjectSectionListAdapter(Context context, List<SectionTileUI> subjectSectionList, boolean isLocked) {
        this.context = context;
        this.subjectSectionList = subjectSectionList;
        this.isLocked = isLocked;
        adapterGroupList = new ArrayList<>();


        for (SectionTileUI sectionTileUI : subjectSectionList) {
            adapterGroupList.add(
                    new AdapterGroup(new QuestionsProgressAdapter(sectionTileUI.questionProgresses)
                            ,new BookMarkQuestionProgressAdapter(sectionTileUI.questionProgresses)));
        }

        inflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return subjectSectionList.size();
    }

    @Override
    public SectionTileUI getItem(int i) {
        return subjectSectionList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return subjectSectionList.get(i).sectionId;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        MultiProgressBar multiProgressBar = null;
        AdapterGroup adapterGroup = adapterGroupList.get(i);

        boolean isSetupProgressBar = false;

        if(view == null) {
            view = inflater.inflate(R.layout.subject_section_list_tile, viewGroup, false);
            multiProgressBar = (MultiProgressBar) view.findViewById(R.id.progress_bar);
            multiProgressBar.setAdapter(currentFilter==Filter.BOOK_MARK?
                    adapterGroup.bookMarksProgressAdapter:
                    adapterGroup.questionsProgressAdapter, MultiProgressBar.AnimateType.BEGIN
            );
            isSetupProgressBar = true;
        }


        view.findViewById(R.id.lock_iv).setVisibility(isLocked?View.VISIBLE:View.GONE);

         multiProgressBar = multiProgressBar==null?(MultiProgressBar) view.findViewById(R.id.progress_bar)
                 :multiProgressBar;
        if(!isSetupProgressBar && currentFilter==Filter.BOOK_MARK && multiProgressBar.getAdapter() != adapterGroupList.get(i).bookMarksProgressAdapter)
        {
            multiProgressBar.setAdapter(adapterGroup.bookMarksProgressAdapter, MultiProgressBar.AnimateType.CONTINUE);
            isSetupProgressBar = true;
        }
        else if(!isSetupProgressBar && currentFilter!=Filter.BOOK_MARK && multiProgressBar.getAdapter() == adapterGroup.bookMarksProgressAdapter)
        {
            isSetupProgressBar = true;
            if(currentFilter==Filter.UNANSWRED)
            multiProgressBar.setAdapter(adapterGroup.questionsProgressAdapter, MultiProgressBar.AnimateType.CONTINUE);
            else
                multiProgressBar.setAdapterAndShowOneProgress(adapterGroup.questionsProgressAdapter,filterToProgressType(currentFilter), MultiProgressBar.AnimateType.CONTINUE);
        }

        ((TextView)view.findViewById(R.id.title_tv)).setText(subjectSectionList.get(i).title);
        TextView questionTV = ((TextView)view.findViewById(R.id.questionNo_tv));


        if(!isSetupProgressBar)
        {
            if( currentFilter !=Filter.BOOK_MARK) {
                if(currentFilter != Filter.UNANSWRED) {
                    multiProgressBar.showOnlyOneProgress(filterToProgressType(currentFilter));
                }
                else {
                    multiProgressBar.showAllProgress();
                }
            }
        }

        int questionCount = currentFilter == Filter.BOOK_MARK?
                adapterGroup.bookMarksProgressAdapter.getBookMarkQuestionCount():
                adapterGroup.questionsProgressAdapter.getQuestionCount(filterToProgressType(currentFilter));

        questionTV.setText("Questions "+questionCount);


        return view;
    }

    public List<QuestionProgress> getQuestionsForfilter(int position)
    {
       if(currentFilter!=Filter.BOOK_MARK)
        return adapterGroupList.get(position)
                .questionsProgressAdapter
                .getQuestions(filterToProgressType(currentFilter));

        return adapterGroupList.get(position)
                .bookMarksProgressAdapter
                .getBookMarkQuestionList();
    }

    public List<SectionTileUI> getSubjectSectionList() {
        return subjectSectionList;
    }

    public void setFilter(Filter filter)
    {
        currentFilter = filter;
        notifyDataSetChanged();
    }

    private static List<Integer> getBookMarkList(List<Integer> indexs)
    {
        QuestionService questionService = QuestionService.getService();
        List<Integer> bookMarks = new ArrayList<>();
        for (Integer index : indexs) {
            if(questionService.getQuestionForID(index).isBookMarked())
            {
                bookMarks.add(index);
            }
        }
        return bookMarks;
    }

    public static ProgressType filterToProgressType(Filter filter)
    {
        switch (filter)
        {
            case DONT_KNOW:
                return ProgressType.DONT_KNOW;
            case KNOW:
                return ProgressType.KNOW;
            case SOME_HOW_KNOW:
                return ProgressType.SOME_HOW_KNOW;
            case UNANSWRED:
                return ProgressType.UNANSWRED;
        }
        return null;
    }

    public static Filter progressTypeToFilter(ProgressType progressType)
    {
        switch (progressType)
        {
            case DONT_KNOW:
                return Filter.DONT_KNOW;
            case SOME_HOW_KNOW:
                return Filter.SOME_HOW_KNOW;
            case KNOW:
                return Filter.KNOW;
            case UNANSWRED:
                return Filter.UNANSWRED;
        }
        return null;
    }

    public enum Filter
    {
        UNANSWRED,DONT_KNOW,SOME_HOW_KNOW,KNOW,BOOK_MARK
    }

    @Override
    public void notifyDataSetChanged() {
        for (AdapterGroup adapterGroup : adapterGroupList) {
            adapterGroup.questionsProgressAdapter.referesh();
            adapterGroup.bookMarksProgressAdapter.referesh();
        }
        super.notifyDataSetChanged();

    }

    private class AdapterGroup
    {
        public QuestionsProgressAdapter questionsProgressAdapter;
        public BookMarkQuestionProgressAdapter bookMarksProgressAdapter;

        public AdapterGroup(QuestionsProgressAdapter questionsProgressAdapter, BookMarkQuestionProgressAdapter bookMarksProgressAdapter) {
            this.questionsProgressAdapter = questionsProgressAdapter;
            this.bookMarksProgressAdapter = bookMarksProgressAdapter;
        }
    }
}

