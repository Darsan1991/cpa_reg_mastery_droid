package fiverr.com.cparegmastery.Adapter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import fiverr.com.cparegmastery.enums.ProgressType;
import fiverr.com.cparegmastery.model.QuestionProgress;

/**
 * Created by Darsan on 5/8/2017.
 */

public abstract class ProgressAdapter {
    protected List<QuestionProgress> totalQuestionLists;
    protected HashMap<ProgressType,ArrayList<QuestionProgress>> progressTypeArrayListHashMap;

    protected ProgressAdapterListner listner;

    public  ProgressAdapter(List<QuestionProgress> totalQuestionLists)
    {
        this.totalQuestionLists = totalQuestionLists;
        progressTypeArrayListHashMap = new HashMap<>();
    }


    public abstract void referesh();


    public int getTotalQuestionCount()
    {
        return totalQuestionLists.size();
    }

    public List<QuestionProgress> getTotalQuestions()
    {
        return totalQuestionLists;
    }

    public int getQuestionCount(ProgressType progressType)
    {
        return progressTypeArrayListHashMap.get(progressType).size();
    }

    public List<QuestionProgress> getQuestions(ProgressType progressType)
    {
        return progressTypeArrayListHashMap.get(progressType);
    }

    public void setListner(ProgressAdapterListner listner) {
        this.listner = listner;
    }

    public void removeListners()
    {
        this.listner = null;
    }

    public interface ProgressAdapterListner
    {
        void onDataChanged();
    }
}
