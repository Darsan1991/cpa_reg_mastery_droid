package fiverr.com.cparegmastery.Adapter.list;

import android.content.Context;

import java.util.List;

import fiverr.com.cparegmastery.R;

/**
 * Created by Darsan on 4/17/2017.
 */

public class MultiChoiseQuizListAdapter extends MultiChoiceBaseListAdapter {


    public MultiChoiseQuizListAdapter(Context context,List<String> answerList) {
       super(context,answerList);
    }


    public void setSelectedIndex(int i)
    {
        int lastSelectedIndex = selectedIndex;
        super.setSelectedIndex(i);
        if(lastSelectedIndex>=0)
        {
            if(indexToViewHashMap.containsKey(lastSelectedIndex))
            {
                ((fiverr.com.cparegmastery.view.RadioButton)indexToViewHashMap.get(lastSelectedIndex).findViewById(R.id.radio_btn)).setAsSelected(false);
            }
        }

        if(selectedIndex>=0)
        ((fiverr.com.cparegmastery.view.RadioButton)indexToViewHashMap.get(selectedIndex).findViewById(R.id.radio_btn)).setAsSelected(true);

    }

}
