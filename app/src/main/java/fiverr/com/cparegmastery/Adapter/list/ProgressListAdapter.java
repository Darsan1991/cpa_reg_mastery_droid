package fiverr.com.cparegmastery.Adapter.list;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.HashMap;
import java.util.List;

import fiverr.com.cparegmastery.R;
import fiverr.com.cparegmastery.fragment.ProgressFragment;
import fiverr.com.cparegmastery.view.MultiProgressBar;
import fiverr.com.cparegmastery.viewModel.ProgressTileUI;

/**
 * Created by Darsan on 4/30/2017.
 */

public class ProgressListAdapter extends BaseAdapter implements ProgressFragment.ListItemVisableListner {

    private HashMap<Integer,View> listTileHashMap = new HashMap<>();
    private List<ProgressTileUI> progressTileUIList;
    private Context context;
    private LayoutInflater layoutInflater;

    public ProgressListAdapter(Context context,List<ProgressTileUI> progressTileUIList) {
        this.progressTileUIList = progressTileUIList;
        this.context = context;
        layoutInflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return progressTileUIList.size();
    }

    @Override
    public ProgressTileUI getItem(int position) {
        return progressTileUIList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return progressTileUIList.size();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if(convertView == null)
        {
            convertView = layoutInflater.inflate(R.layout.progress_list_tile,parent,false);
        }

        listTileHashMap.put(position,convertView);
        ProgressTileUI progressTileUI = progressTileUIList.get(position);

        ((TextView)convertView.findViewById(R.id.title_tv)).setText(progressTileUI.getTitle());
        ((TextView)convertView.findViewById(R.id.percentage_tv)).setText(progressTileUI.getPercentage()+"%");

        TextView statusTV = (TextView) convertView.findViewById(R.id.status_tv);
        if(progressTileUI.isAnyQuestionAnswred()) {
            statusTV.setText(progressTileUI.getPercentage() == 0 ?
                    "WEAK"
                    : (progressTileUI.getPercentage() == 100 ?
                    "STRONG"
                    : ""));
        }
        else
        {
            statusTV.setText("");
        }

       MultiProgressBar progressBar = (MultiProgressBar)convertView.findViewById(R.id.progress_bar);
        progressBar.clearAnimation();

        return convertView;
    }


    @Override
    public void onBecomeVisableAtStart(List<Integer> list) {
        for (Integer pos : list) {
            MultiProgressBar progressBar = (MultiProgressBar)listTileHashMap.get(pos).findViewById(R.id.progress_bar);
            progressBar.setAdapter(progressTileUIList.get(pos).getProgressAdapter(), MultiProgressBar.AnimateType.BEGIN);
        }
    }

    @Override
    public void onItemBecomeVisable(List<Integer> list) {
        for (Integer position : list) {
            MultiProgressBar progressBar = (MultiProgressBar)listTileHashMap.get(position).findViewById(R.id.progress_bar);
            progressBar.setAdapter(progressTileUIList.get(position).getProgressAdapter(), MultiProgressBar.AnimateType.BEGIN);
        }

    }

    @Override
    public void onItemHide(List<Integer> list) {
        for (Integer position : list) {
            MultiProgressBar progressBar = (MultiProgressBar)listTileHashMap.get(position).findViewById(R.id.progress_bar);
            progressBar.clearAnimation();
        }
    }
}
