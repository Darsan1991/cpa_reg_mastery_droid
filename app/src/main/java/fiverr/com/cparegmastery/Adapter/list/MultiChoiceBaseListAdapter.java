package fiverr.com.cparegmastery.Adapter.list;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.util.HashMap;
import java.util.List;

import fiverr.com.cparegmastery.R;

/**
 * Created by Darsan on 4/21/2017.
 */

public abstract class MultiChoiceBaseListAdapter  extends BaseAdapter implements AdapterView.OnItemClickListener {
   protected List<String> answerList;
   protected Context context;
   protected LayoutInflater inflater;
   protected HashMap<Integer,View> indexToViewHashMap = new HashMap<>();
   protected int selectedIndex = -1;
    protected AdapterView.OnItemClickListener itemClickListener;


    public MultiChoiceBaseListAdapter(Context context,List<String> answerList) {
        this.answerList = answerList;
        this.context = context;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return answerList.size();
    }

    @Override
    public String getItem(int i) {
        return answerList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }




    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        if(view==null)
        {
            view = inflater.inflate(R.layout.choise_quiz_tile,viewGroup,false);
        }


        ((TextView)view.findViewById(R.id.quiz_text)).setText(answerList.get(i));
        indexToViewHashMap.put(i,view);
        if(selectedIndex==i)
            setSelectedIndex(i);
        else
            view.setBackgroundResource(R.color.normal_list_tile_color);

        return view;
    }


    public void setSelectedIndex(int i)
    {
        if(selectedIndex>=0)
        {
            if(indexToViewHashMap.containsKey(selectedIndex))
            {
                indexToViewHashMap.get(selectedIndex).setBackgroundResource(R.color.normal_list_tile_color);
            }
        }
        selectedIndex = i;
        if(selectedIndex>=0)
            indexToViewHashMap.get(selectedIndex).setBackgroundResource(R.color.selected_listTile_color);

    }

    public void clearSelection()
    {
        setSelectedIndex(-1);
    }

    public void setListView(ListView listView)
    {
        listView.setOnItemClickListener(this);
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        setSelectedIndex(i);
        if(itemClickListener!=null)
            itemClickListener.onItemClick(adapterView,view,i,l);
    }

    public void setItemClickListener(AdapterView.OnItemClickListener itemClickListener) {
        this.itemClickListener = itemClickListener;
    }
}
