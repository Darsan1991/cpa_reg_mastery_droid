package fiverr.com.cparegmastery.Adapter.list;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import fiverr.com.cparegmastery.R;
import fiverr.com.cparegmastery.view.RadioButton;

/**
 * Created by Darsan on 4/21/2017.
 */

public class MultiChoiceResultListAdapter extends MultiChoiceBaseListAdapter {
   private int correctIndex;

    private boolean isCorrect;

    public MultiChoiceResultListAdapter(Context context, List<String> answerList, int correctIndex, int selectedIndex) {
        super(context, answerList);
        this.correctIndex = correctIndex;
        this.selectedIndex = selectedIndex;
        isCorrect = selectedIndex == correctIndex;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        View v = super.getView(i, view, viewGroup);
        if(i==correctIndex)
        {
            ((RadioButton)v.findViewById(R.id.radio_btn)).setCurrentType(RadioButton.Type.CORRECT);
        }
        else if(!isCorrect && i==selectedIndex)
        {
            ((RadioButton)v.findViewById(R.id.radio_btn)).setCurrentType(RadioButton.Type.WRONG);
        }
        else
        {
            ((RadioButton)v.findViewById(R.id.radio_btn)).setCurrentType(RadioButton.Type.SELECTION);
        }
        return v;
    }

    @Override
    public boolean isEnabled(int position) {
        return super.isEnabled(position);
    }
}
