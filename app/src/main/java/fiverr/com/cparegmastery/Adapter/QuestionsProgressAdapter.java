package fiverr.com.cparegmastery.Adapter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import fiverr.com.cparegmastery.enums.ProgressType;
import fiverr.com.cparegmastery.model.QuestionProgress;

/**
 * Created by Darsan on 4/24/2017.
 */

public class QuestionsProgressAdapter extends ProgressAdapter{

    public QuestionsProgressAdapter(List<QuestionProgress> totalQuestionLists) {
        super(totalQuestionLists);
        progressTypeArrayListHashMap = new HashMap<>();
        for (ProgressType progressType : ProgressType.values()) {
            progressTypeArrayListHashMap.put(progressType,new ArrayList<QuestionProgress>());
        }
        for (QuestionProgress questionProgress : totalQuestionLists) {
            if(questionProgress==null)
                continue;
            progressTypeArrayListHashMap.get(questionProgress.getProgressType()).add(questionProgress);
        }
    }




    public void referesh()
    {
        boolean isDataChanged=false;
        for (ProgressType progressType : ProgressType.values()) {
           List<QuestionProgress> questionProgressList = progressTypeArrayListHashMap.get(progressType);
            for (int i = 0; i < questionProgressList.size(); i++) {
                QuestionProgress selectedQuestion = questionProgressList.get(i);
                if(selectedQuestion.getProgressType() != progressType)
                {
                    if(!isDataChanged)
                        isDataChanged = true;
                    progressTypeArrayListHashMap.get(selectedQuestion.getProgressType()).add(selectedQuestion);
                    questionProgressList.remove(i);
                    i--;
                }
            }
        }
        if(isDataChanged && listner!=null)
            listner.onDataChanged();

    }




}
