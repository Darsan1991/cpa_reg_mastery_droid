package fiverr.com.cparegmastery;

import android.support.annotation.NonNull;

import com.facebook.AccessToken;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FacebookAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;

import fiverr.com.cparegmastery.Presistance.FirebaseQuestionPresistance;
import fiverr.com.cparegmastery.interfaces.OnTaskCompleteListner;
import fiverr.com.cparegmastery.utils.FirebaseUtils;

/**
 * Created by Darsan on 5/13/2017.
 */

public class FirebaseAccountHandler implements AccountHandler {

    FirebaseAuth mAuth;

    public FirebaseAccountHandler()
    {
        mAuth = FirebaseAuth.getInstance();
    }

    @Override
    public void signUpWithFacebook(AccessToken accessToken, final OnTaskCompleteListner listner) {
        AuthCredential credential = FacebookAuthProvider.getCredential(accessToken.getToken());
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(
                        new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if(task.isSuccessful())
                        {
                            if(listner!=null)
                                listner.onComplete(OnTaskCompleteListner.Result.SUCCESS);
                        }
                        else
                        {
                            if(listner!=null)
                                listner.onComplete(OnTaskCompleteListner.Result.FAILED);
                        }

                    }
                });
    }

    @Override
    public void signUpWithEmail(String emailAddress, String password, final OnTaskCompleteListner listner) {

        mAuth.createUserWithEmailAndPassword(emailAddress,password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if(task.isSuccessful())
                {
                    if(listner!=null)listner.onComplete(OnTaskCompleteListner.Result.SUCCESS);
                }
                else
                {
                    if(listner!=null)listner.onComplete(OnTaskCompleteListner.Result.FAILED);
                }
            }
        });
    }

    @Override
    public void signInWithEmail(String emailAddress, String password, final OnTaskCompleteListner listner) {
        mAuth.signInWithEmailAndPassword(emailAddress,password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if(task.isSuccessful())
                {
                    if(listner!=null)listner.onComplete(OnTaskCompleteListner.Result.SUCCESS);
                }
                else
                {
                    if(listner!=null)listner.onComplete(OnTaskCompleteListner.Result.FAILED);
                }
            }
        });

    }

    @Override
    public void signOut() {
        FacebookHandler handler = new FacebookHandler();
        if(handler.isLogIn())
        handler.logOut();
        mAuth.signOut();
    }

    @Override
    public void resetPassword(final OnTaskCompleteListner listner) {
        mAuth.sendPasswordResetEmail(getEmailAddress()).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if(listner!=null)
                    listner.onComplete(task.isSuccessful()? OnTaskCompleteListner.Result.SUCCESS: OnTaskCompleteListner.Result.FAILED);
            }
        });
    }

    @Override
    public String getEmailAddress() {
        return mAuth.getCurrentUser().getEmail();
    }

    @Override
    public void updatePassword(String newPassword, final OnTaskCompleteListner listner) {
        mAuth.getCurrentUser().updatePassword(newPassword).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if(listner!=null)
                    listner.onComplete(task.isSuccessful()? OnTaskCompleteListner.Result.SUCCESS: OnTaskCompleteListner.Result.FAILED);
            }
        });
    }

    @Override
    public void resetAccount(final OnTaskCompleteListner listner) {
        new FirebaseQuestionPresistance().reset(listner);
    }

    @Override
    public void isPremiumUnlocked(final OnPremiumQueryListner listner) {
        final DatabaseReference premiumReference = FirebaseUtils.getPremiumReference();

        premiumReference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if(dataSnapshot.getValue()!=null)
                {
                    boolean isPremium = (boolean)dataSnapshot.getValue();
                    if(listner!=null)
                        listner.onSuccess(isPremium);
                }
                else
                {
                    if(listner!=null)
                        listner.onSuccess(false);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                if(listner!=null)
                    listner.onFailed();
            }
        });
    }

    @Override
    public void unlockPremiumLocked(boolean isUnlock, final OnTaskCompleteListner listner) {
        FirebaseUtils.getPremiumReference().setValue(isUnlock).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if(listner!=null)
                    listner.onComplete(task.isSuccessful()? OnTaskCompleteListner.Result.SUCCESS: OnTaskCompleteListner.Result.FAILED);
            }
        });
    }
}
